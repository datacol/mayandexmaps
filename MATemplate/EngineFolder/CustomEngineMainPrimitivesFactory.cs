﻿using MAPrimitives.EngineFolder;
using MAPrimitives.EngineMainPrimitivesFactoryFolder;
using MAPrimitives.OptionFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATemplate.EngineFolder
{
    public class CustomEngineMainPrimitivesFactory : EngineMainPrimitivesFactory
    {
        public override IEngine CreateEngine()
        {
            return new CustomEngine();
        }

        public override Options GenerateOptionObject()
        {
            return new CustomOptions();
        }
    }
}
