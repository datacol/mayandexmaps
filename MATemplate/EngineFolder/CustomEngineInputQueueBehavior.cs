﻿using MAPrimitives.Misc;
using MAPrimitives.QueueFolder.FillQueueBehaviourFolder;
using MAPrimitives.QueueFolder.QueueItemFolder;
using MATemplate.CoreFolder;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MATemplate.EngineFolder
{
    public class CustomEngineInputQueueBehavior : IFillInputQueueBehaviour
    {
        private CustomEngine CustomEngine;

        public CustomEngineInputQueueBehavior(CustomEngine _CustomEngine)
        {
            CustomEngine = _CustomEngine;
        }

        public void Fill()
        {
            List<QueueItemBase> itemList = new List<QueueItemBase>();

            List<string> cities = StringHandling.SplitString((CustomEngine.OptionWrapper.Options as CustomOptions).City, new char[] {';'});

            foreach (string city in cities)
            {
                if (CustomEngine.CityObjectDict.ContainsKey(city)) continue;

                YandexMapsApi yandexMapsApi = new YandexMapsApi(CustomEngine.OptionWrapper, (CustomEngine.OptionWrapper.Options as CustomOptions).QueryString, city);

                yandexMapsApi.radiuskm = Convert.ToInt32(Regex.Replace((CustomEngine.OptionWrapper.Options as CustomOptions).Radius, "[^\\d]", ""));

                if ((CustomEngine.OptionWrapper.Options as CustomOptions).UseProxy)
                {
                    char[] array1 = {';', '\n', '\r'};

                    yandexMapsApi.SetProxyList(StringHandling.SplitString((CustomEngine.OptionWrapper.Options as CustomOptions).ProxyList, array1));
                }

                CustomEngine.CityObjectDict.Add(city, yandexMapsApi);

                itemList.Add(new YandexMapsParserQueueItem(city, "", false));
            }

            CustomEngine.AddItemsToQueue(itemList);
        }
    }
}