﻿using MAPrimitives.QueueFolder.QueueItemFolder;

namespace MATemplate.EngineFolder
{
    public class YandexMapsParserQueueItem : QueueItemBase
    {
        public string Coord;
        public string City;
        public bool ProcessCoord;

        public YandexMapsParserQueueItem()
        {
            ProcessCoord = false;
            Coord = "";
            City = "";
        }

        public YandexMapsParserQueueItem(string city, string coord, bool processCoord)
        {
            City = city;
            Coord = coord;
            ProcessCoord = processCoord;
        }

        public override string GetItemUniqueKey()
        {
            return City + Coord;
        }
    }
}