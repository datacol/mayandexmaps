﻿using System.Collections.Generic;
using System.Data;
using MAPrimitives.EngineFolder;
using MAPrimitives.LoadManagementFolder.CacherFolder;
using MAPrimitives.QueueFolder.QueueItemFolder;
using MATemplate.CoreFolder;

namespace MATemplate.EngineFolder
{
    
    public class CustomEngine : MAEngine
    {
        //internal bool Ed;
        //:IEngine
        public Dictionary<string, YandexMapsApi> CityObjectDict = new Dictionary<string, YandexMapsApi>();

        //YandexMapsApi YandexMapsApi;

        PagecodeCacher _cache;
        public CustomEngine()
            : base()
        {
            InputQueueBehavior = new CustomEngineInputQueueBehavior(this);
            Stats = new CustomStats();

            //Queue = new SimpleQueue();
            //(Queue as SimpleQueue).ElementCanBeAddedBehavior = new NonUniqueElementCanBeAddedBehavior();
        }

        public override void InitSpecific() 
        {
            
        }
       
        public override void SetDL(object _DL)
        {            
            (OptionWrapper.Options as CustomOptions).MaxResults = (int)_DL;
        }

        public override void ProcessItemSpecific(object item)
        {
            if (!(item as YandexMapsParserQueueItem).ProcessCoord)
            {
                Logger.AddImportantInfoEventToLog("Получение координат для города",
                    (item as YandexMapsParserQueueItem).City);

                List<string> coords = CityObjectDict[(item as YandexMapsParserQueueItem).City].GetData();

                List<QueueItemBase> itemList = new List<QueueItemBase>();

                foreach (string coord in coords)
                {
                    itemList.Add(new YandexMapsParserQueueItem((item as YandexMapsParserQueueItem).City, coord, true));
                }

                //CreateResultTable(YandexMapsApi.dtCityReq);

                foreach (DataRow row in CityObjectDict[(item as YandexMapsParserQueueItem).City].dtCityReq.Rows)
                {
                    lock (StatsLocker)
                    {
                        if (LimitsReached()) break;
                        (Stats as CustomStats).ResultsCount++;
                    }

                    AddResultsToUIDelegate(row);
                    //AddRowToResultTable(row.ItemArray);
                }

                AddItemsToQueue(itemList);
            }
            else
            {
                Logger.AddImportantInfoEventToLog("Обработка точки координат", (item as YandexMapsParserQueueItem).Coord);

                DataTable dt = CityObjectDict[(item as YandexMapsParserQueueItem).City].
                    ProcessLinkWithPoint((item as YandexMapsParserQueueItem).Coord);

                //CreateResultTable(dt);

                foreach (DataRow row in dt.Rows)
                {
                    lock (StatsLocker)
                    {
                        if (LimitsReached()) break;
                        (Stats as CustomStats).ResultsCount++;
                    }
                    AddResultsToUIDelegate(row);
                    //AddRowToResultTable(row.ItemArray);
                }
                
            }

        }

        public override bool LimitsReached()
        {
            var customOptions = OptionWrapper.Options as CustomOptions;
            var customStats = Stats as CustomStats;
            if (customStats != null && (customOptions != null && (customOptions.MaxResults > 0 && customStats.ResultsCount >= customOptions.MaxResults)))
            {
                return true;
            }
            return false;
        }
    }
}
