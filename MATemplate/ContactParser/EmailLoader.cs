﻿using System;
using System.Collections.Generic;
using System.Linq;
using LowLevel;
using MAPrimitives.Misc;

namespace MATemplate.ContactParser
{
    public class EmailLoader
    {
        private string _url;
        private int _depth;
        private readonly DatacolHttp _http = new DatacolHttp(15000);
        public EmailLoader(string url, int depth)
        {
            _url = url;
            _depth = depth;
        }

        public List<string> Load()
        {
            Dictionary<string, object> outDictParams;
            string error;
            var pageContent = _http.request(_url, _url, out outDictParams, out error);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }

            return GetEmails(pageContent);
        }

        public List<string> GetEmails(string pageContent)
        {
            var retValue = new List<string>();
            var emails = StringHandling.Matches(pageContent, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            retValue.AddRange(emails.Where(email => !string.IsNullOrEmpty(email)));
            return retValue.Where(r => !r.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) && !r.EndsWith(".png", StringComparison.OrdinalIgnoreCase)).Distinct().ToList();
        }
    }
}