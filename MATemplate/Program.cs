﻿
using MAForms.ExtraForms;
using MAForms.Misc;
using MAForms.Misc.ButtonGenerationFolder;
using MAPrimitives.EngineFolder;
using MAPrimitives.Misc;
using MATemplate.EngineFolder;
using MATemplate.Localization.LocalizationResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace MATemplate
{
    static class Program
    {
        static StarterParameters starterParameters = new StarterParameters();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            

            HardcodedParameters hardcodedParameters = new HardcodedParameters(
                "Yandex Maps Parser", "3.20", "parser-yandex-kart",
                "http://parser-yandex-kart.ru", "lic.txt",
                false, 50 ,CustomCommon.DemoResults);

            hardcodedParameters.ASupported = true;

            hardcodedParameters.BuyLink = hardcodedParameters.Website + "/license/buy";
            hardcodedParameters.ActivationResetUrl = "http://parser-yandex-kart.ru/sbros-aktivacii/?email=email&code=codelicense";
            starterParameters.HardcodedParameters = hardcodedParameters;
            starterParameters.CMDParameters = new CMDParameters(args);//todo
            starterParameters.ExePath = Assembly.GetExecutingAssembly().Location;

            starterParameters.CustomOptionType = typeof(CustomOptions);
            starterParameters.EngineMainPrimitivesFactory = new CustomEngineMainPrimitivesFactory();
            starterParameters.ApplicationGlyphImagePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "Images", "app_glyph.png");
            starterParameters.ApplicationTrayIconPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "Images", "app_icon_.ico");

            starterParameters.BuyButtonHandler = BuyButtonHandler;

            starterParameters.ButtonGenerationInfoList = new List<ButtonGenerationInfo>();

            //ButtonGenerationInfo bgi = new ButtonGenerationInfo();
            //bgi.Caption = "sfd";
            //bgi.ImageLocalPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            //    "Images", "app_glyph.png");
            //bgi.Handler = xxx;

            //starterParameters.ButtonGenerationInfoList.Add(bgi);

            StarterForm starter = new StarterForm(starterParameters);
            
            Application.Run(starter);
        }



        //private static void xxx(object sender, EventArgs e)
        //{
        //    MessageBox.Show("asdasd");
        //}


        private static void BuyButtonHandler(object sender, EventArgs e)
        {
            //MessageBox.Show("Для покупки программы, пожалуйста, отправьте письмо на email поддержки: support@parser-yandex-kart.ru");
            System.Diagnostics.Process.Start(starterParameters.HardcodedParameters.BuyLink);
        }
        
    }

    
}
