﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Limilabs.Proxy;
//using Limilabs.Proxy.Exceptions;

namespace MATemplate.CoreFolder
{

    public class ProxyInfo2
    {
        public ProxyInfo2(string _server, int _port, string _login = "", string _pass = "", ProxyType _type = ProxyType.HTTP)
        {
            server = _server;
            port = _port;
            login = _login;
            pass = _pass;
            type = _type;
        }
        public enum ProxyType { NO = 0, HTTP, SOCKS4, SOCKS5 };

        //тип прокси
        public ProxyType type;


        //Сервер
        public string server;

        //Порт
        public int port;

        //Логин
        public string login;

        //Пароль
        public string pass;

        //Таймаут обращения к прокси серверу (количество милисекунд ожидания ответа)
        //The existing timeout properties for the various classes/objects also apply to the communications with the proxies


    }
}
