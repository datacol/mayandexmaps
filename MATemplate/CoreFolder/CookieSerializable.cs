using System;

namespace MATemplate.CoreFolder
{
    public class CookieSerializable
    {
        public CookieSerializable()
        {

        }

        public string Name { get; set; }

        public string Domain { get; set; }

        public string Path { get; set; }

        public string Value { get; set; }

        public DateTime Expires { get; set; }
    }
}