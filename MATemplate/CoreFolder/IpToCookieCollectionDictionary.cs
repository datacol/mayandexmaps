﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace MATemplate.CoreFolder
{
    public class IpToCookieCollectionDictionary
    {
        //можно сериализовать cc для конкретного, а потом все cc для данного iP подгружать

        #region Properties
        public static string noproxyindex = "noproxy";

        Dictionary<string, CookieCollection> dictionary = new Dictionary<string, CookieCollection>();

        private bool mSaveToFile;

        public bool SaveToFile
        {
            get { return mSaveToFile; }
            set { mSaveToFile = value; }
        }

        private string mSaveCookiesFolderPath;

        public string SaveCookiesFolderPath
        {
            get { return mSaveCookiesFolderPath; }
            set { mSaveCookiesFolderPath = value; }
        }
        #endregion


        #region ctor
        public IpToCookieCollectionDictionary(bool _mSaveToFile, string _mSaveCookiesFolderPath)
        {

            mSaveToFile = _mSaveToFile;

            mSaveCookiesFolderPath = _mSaveCookiesFolderPath;


            if (mSaveToFile)
            {
                Deserialize();
            }
        }
        #endregion

        #region Methods

        #region dictionary handling
        public void addToDictionary(string ip, CookieCollection cc)
        {
            ip = Regex.Replace(ip, "http://","", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            ip = Regex.Replace(ip, "/", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            ip = Regex.Replace(ip, ":.*?$", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);

            if (!dictionary.ContainsKey(ip))
            {
                dictionary.Add(ip, cc);
            }
            else
            {
                foreach (Cookie c in cc)
                {
                    dictionary[ip].Add(c);
                }
            }

        }

        public void removeFromDictionary(string ip)
        {
            ip = Regex.Replace(ip, "http://", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            ip = Regex.Replace(ip, "/", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            ip = Regex.Replace(ip, ":.*?$", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);

            if (!dictionary.ContainsKey(ip))
            {
                dictionary.Remove(ip);
            }
            

        }
        public CookieCollection getFromDictionary(string ip)
        {

            try
            {
                return dictionary[ip];
            }
            catch
            {
                return new CookieCollection();
            }

        }
        #endregion

        #region ser/deser


        public void IpToCookieCollectionDictionarySerialize()
        {
            if (mSaveToFile)
            {
                Serialize();
            }
        }

        private void Serialize()
        {

            foreach (string ip in dictionary.Keys)
            {
                CookieCollection cc = dictionary[ip];

                Dictionary<string, List<CookieSerializable>> dictionaryDomainToCookieList = new Dictionary<string, List<CookieSerializable>>();

                foreach (Cookie c in cc)
                {
                    CookieSerializable item = new CookieSerializable();
                    item.Domain = c.Domain;
                    item.Expires = c.Expires;
                    item.Name = c.Name;
                    item.Path = c.Path;
                    item.Value = c.Value;

                    if (!dictionaryDomainToCookieList.Keys.Contains(c.Domain))
                    {
                        dictionaryDomainToCookieList.Add(c.Domain, new List<CookieSerializable>());
                    }

                    dictionaryDomainToCookieList[c.Domain].Add(item);
                }

                foreach (string domain in dictionaryDomainToCookieList.Keys)
                {
                    XmlSerializer serializer1 = new XmlSerializer(typeof(List<CookieSerializable>));
                    string filename = Path.Combine(mSaveCookiesFolderPath, ip + "_" + domain + ".coo");

                    if (File.Exists(filename))
                        File.Delete(filename);
                    TextWriter textWriter1 = new StreamWriter(filename, true);
                    serializer1.Serialize(textWriter1, dictionaryDomainToCookieList[domain]);
                    textWriter1.Close();
                }
            }




        }

        private void Deserialize()
        {
            string[] files = Directory.GetFiles(mSaveCookiesFolderPath, "*.coo", SearchOption.TopDirectoryOnly);

            foreach (string file in files)
            {
                XmlSerializer deserializer1 = new XmlSerializer(typeof(List<CookieSerializable>));
                TextReader textReader1 = new StreamReader(file);

                List<CookieSerializable> listWithCookiesForDomain = (List<CookieSerializable>)deserializer1.Deserialize(textReader1);

                Match m = Regex.Match(Path.GetFileName(file), "^(.*?)_(.*?)\\.coo$");

                string ip = m.Groups[1].Value;

                string domain = m.Groups[2].Value;//тут даже не используется

                CookieCollection cc = new CookieCollection();

                foreach (CookieSerializable cs in listWithCookiesForDomain)
                {
                    Cookie c = new Cookie(cs.Name, cs.Value, cs.Path, cs.Domain);
                    c.Expires = cs.Expires;
                    cc.Add(c);
                }

                addToDictionary(ip, cc);

                textReader1.Close();
            }

        }
        #endregion

        #region to/from list string
        public List<string> getCookieCollectionToListString(string ip)
        {
            List<string> results = new List<string>();

            try
            {
                foreach (Cookie c in dictionary[ip])
                {

                    string line = "";

                    if (c.Domain != "" || c.Path != "")
                    {
                        line = c.Name + "=" + c.Value + ";" + c.Domain + ";" + c.Path;
                    }
                    else
                    {
                        line = c.Name + "=" + c.Value;
                    }
                    results.Add(line);
                }
            }
            catch
            {

            }

            return results;
        }

        public void setCookieCollectionToListString(string ip, List<string> cookieStrings)
        {
            if (ip == "")
            {
                ip = noproxyindex;
            }

            CookieCollection cc = new CookieCollection();

            foreach (string cs in cookieStrings)
            {
                if (Regex.IsMatch(cs, "^(.*?)=(.*?);(.*?);(.*?)$"))
                {
                    Match m = Regex.Match(cs, "^(.*?)=(.*?);(.*?);(.*?)$");
                    Cookie cookie = new Cookie(m.Groups[1].Value, m.Groups[2].Value, m.Groups[4].Value, m.Groups[3].Value);
                    cc.Add(cookie);
                }
                else if (Regex.IsMatch(cs, "^(.*?)=(.*?)$"))
                {
                    Match m = Regex.Match(cs, "^(.*?)=(.*?)$");
                    Cookie cookie = new Cookie(m.Groups[1].Value, m.Groups[2].Value);
                    cc.Add(cookie);
                }
            }

            addToDictionary(ip, cc);
        }
        #endregion

        #endregion
    }
}
