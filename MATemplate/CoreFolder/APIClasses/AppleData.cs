using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class AppleData
    {

        [JsonProperty("acid")]
        public string Acid { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }
    }
}