using System.Collections.Generic;
using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Filter
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("enum")]
        public IList<Enum> Enum { get; set; }

        [JsonProperty("disabled")]
        public bool? Disabled { get; set; }
    }
}