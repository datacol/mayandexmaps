using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class SocialLink
    {

        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}