using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class InternalCategory
    {

        [JsonProperty("class")]
        public string Class { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("InternalCategoryInfo")]
        public InternalCategoryInfo InternalCategoryInfo { get; set; }
    }
}