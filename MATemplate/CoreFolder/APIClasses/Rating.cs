using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Rating
    {

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }
    }
}