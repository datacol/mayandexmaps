﻿using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class BusinessProperties
    {

        [JsonProperty("has_verified_owner")]
        public string HasVerifiedOwner { get; set; }

        [JsonProperty("ssid")]
        public string Ssid { get; set; }
    }
}