using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Chain
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("seoname")]
        public string Seoname { get; set; }
    }
}