using System.Collections.Generic;
using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Data
    {

        [JsonProperty("requestId")]
        public string RequestId { get; set; }

        [JsonProperty("requestSerpId")]
        public string RequestSerpId { get; set; }

        [JsonProperty("requestContext")]
        public string RequestContext { get; set; }

        [JsonProperty("requestQuery")]
        public string RequestQuery { get; set; }

        [JsonProperty("requestCorrectedQuery")]
        public string RequestCorrectedQuery { get; set; }

        [JsonProperty("requestBounds")]
        public IList<IList<double>> RequestBounds { get; set; }

        [JsonProperty("displayType")]
        public string DisplayType { get; set; }

        [JsonProperty("totalResultCount")]
        public int TotalResultCount { get; set; }

        [JsonProperty("items")]
        public IList<Item> Items { get; set; }

        [JsonProperty("bounds")]
        public IList<IList<double>> Bounds { get; set; }

        [JsonProperty("filters")]
        public IList<Filter> Filters { get; set; }

        [JsonProperty("topFilters")]
        public IList<string> TopFilters { get; set; }

        [JsonProperty("categories")]
        public IList<Category> Categories { get; set; }
    }
}