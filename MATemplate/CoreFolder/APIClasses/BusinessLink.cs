using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class BusinessLink
    {

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("aref")]
        public string Aref { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }
}