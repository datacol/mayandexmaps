using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Enum
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}