using System.Collections.Generic;
using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Item
    {

        [JsonProperty("coordinates")]
        public IList<double> Coordinates { get; set; }

        [JsonProperty("requestId")]
        public string RequestId { get; set; }

        [JsonProperty("displayCoordinates")]
        public IList<double> DisplayCoordinates { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("bounds")]
        public IList<IList<double>> Bounds { get; set; }

        [JsonProperty("analyticsId")]
        public string AnalyticsId { get; set; }

        [JsonProperty("logId")]
        public string LogId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("geoId")]
        public int GeoId { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("fullAddress")]
        public string FullAddress { get; set; }

        [JsonProperty("urls")]
        public IList<string> Urls { get; set; }

        [JsonProperty("titleRanges")]
        public IList<IList<int>> TitleRanges { get; set; }

        [JsonProperty("addressDetails")]
        public AddressDetails AddressDetails { get; set; }

        [JsonProperty("phones")]
        public IList<Phone> Phones { get; set; }

        [JsonProperty("categories")]
        public IList<Category> Categories { get; set; }

        [JsonProperty("categorySeoname")]
        public IList<string> CategorySeoname { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("workingTimeText")]
        public string WorkingTimeText { get; set; }

        //[JsonProperty("workingTime")]
        //public IList<IList<>> WorkingTime { get; set; }

        [JsonProperty("timezoneOffset")]
        public int TimezoneOffset { get; set; }

        [JsonProperty("businessLinks")]
        public IList<BusinessLink> BusinessLinks { get; set; }

        [JsonProperty("socialLinks")]
        public IList<SocialLink> SocialLinks { get; set; }

        [JsonProperty("drugs")]
        public IList<object> Drugs { get; set; }

        [JsonProperty("features")]
        public IList<Feature> Features { get; set; }

        [JsonProperty("businessProperties")]
        public BusinessProperties BusinessProperties { get; set; }

        [JsonProperty("seoname")]
        public string Seoname { get; set; }

        [JsonProperty("rating")]
        public Rating Rating { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("links")]
        public IList<object> Links { get; set; }

        [JsonProperty("sources")]
        public IList<Source> Sources { get; set; }

        [JsonProperty("chain")]
        public Chain Chain { get; set; }
    }
}