using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class YandexResult
    {

        [JsonProperty("data")]
        public Data Data { get; set; }
    }
}