using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Feature
    {

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }
}