using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Phone
    {

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }
    }
}