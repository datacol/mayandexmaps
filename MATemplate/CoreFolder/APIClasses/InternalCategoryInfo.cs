using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class InternalCategoryInfo
    {

        [JsonProperty("AppleData")]
        public AppleData AppleData { get; set; }

        [JsonProperty("seoname")]
        public string Seoname { get; set; }
    }
}