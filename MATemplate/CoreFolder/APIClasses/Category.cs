using System.Collections.Generic;
using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Category
    {

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("ranges")]
        public IList<IList<int>> Ranges { get; set; }

        [JsonProperty("seoname")]
        public string Seoname { get; set; }
    }
}