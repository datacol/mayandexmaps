﻿using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class AddressDetails
    {

        [JsonProperty("area")]
        public string Area { get; set; }

        [JsonProperty("locality")]
        public string Locality { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("house")]
        public string House { get; set; }
    }
}