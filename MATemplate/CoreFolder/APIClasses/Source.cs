using Newtonsoft.Json;

namespace MATemplate.CoreFolder.APIClasses
{
    public class Source
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }
}