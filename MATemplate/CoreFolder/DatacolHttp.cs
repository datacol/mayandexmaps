﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using MAPrimitives.Misc;

namespace MATemplate.CoreFolder
{
    public class DatacolHttp
    {
        #region Properties

        #region Basic loading settings
        /// <summary>
        /// Таймаут в миллисекундах
        /// </summary>
        private int mTimeout;

        public int Timeout
        {
            get { return mTimeout; }
            set { mTimeout = value; }
        }

        private bool mAllowAutoRedirect;

        public bool AllowAutoRedirect
        {
            get { return mAllowAutoRedirect; }
            set { mAllowAutoRedirect = value; }
        }

        #endregion

        #region Attempts limits

        private int mAttemptsToLoad;

        public int AttemptsToLoad
        {
            get { return mAttemptsToLoad; }
            set { mAttemptsToLoad = value; }
        }

        private int mAttemptsToDownloadFile;

        public int AttemptsToDownloadFile
        {
            get { return mAttemptsToDownloadFile; }
            set { mAttemptsToDownloadFile = value; }
        }

        #endregion

        #region Cookie related

        //todo read chrome or moz cookies http://www.codeproject.com/Articles/330142/Cookie-Quest-A-Quest-to-Read-Cookies-from-Four-Pop

        private bool mHandleCookies;

        public bool HandleCookies
        {
            get { return mHandleCookies; }
            set { mHandleCookies = value; }
        }

        public volatile object cookieLocker = new object();

        private bool mSaveCookiesToFile;

        public bool SaveCookiesToFile
        {
            get { return mSaveCookiesToFile; }
            set { mSaveCookiesToFile = value; }
        }

        IpToCookieCollectionDictionary ipToCookieDict;

        private List<string> mAuthCookies;

        public List<string> AuthCookies
        {
            get { return mAuthCookies; }
            set { mAuthCookies = value; }
        }


        #endregion

        #region Runtime variables

        private WebProxy mProxy;

        public WebProxy Proxy
        {
            get { return mProxy; }
            set { mProxy = value; }
        }

        private string mUserAgent;

        public string UserAgent
        {
            get { return mUserAgent; }
            set { mUserAgent = value; }
        }

        #region runtime counters for anonymity


        public volatile object countersLocker = new object();


        private int mProxyListCounter;

        public int ProxyListCounter
        {
            get { return mProxyListCounter; }
            set { mProxyListCounter = value; }
        }

        private int mUserAgentCounter;

        public int UserAgentCounter
        {
            get { return mUserAgentCounter; }
            set { mUserAgentCounter = value; }
        }


        private int mReferersListCounter;

        public int ReferersListCounter
        {
            get { return mReferersListCounter; }
            set { mReferersListCounter = value; }
        }

        #endregion


        #endregion

        #region Anonymous lists handling

        private List<WebProxy> mProxyList;

        public List<WebProxy> ProxyList
        {
            get { return mProxyList; }
            set { mProxyList = value; }
        }

        private bool mProxyRandomly;

        public bool ProxyRandomly
        {
            get { return mProxyRandomly; }
            set { mProxyRandomly = value; }
        }

        private List<string> mUserAgentList;

        public List<string> UserAgentList
        {
            get { return mUserAgentList; }
            set { mUserAgentList = value; }
        }

        private bool mUserAgentsRandomly;

        public bool UserAgentsRandomly
        {
            get { return mUserAgentsRandomly; }
            set { mUserAgentsRandomly = value; }
        }

        private List<string> mRefererList;

        public List<string> RefererList
        {
            get { return mRefererList; }
            set { mRefererList = value; }
        }

        private bool mReferersRandomly;

        public bool ReferersRandomly
        {
            get { return mReferersRandomly; }
            set { mReferersRandomly = value; }
        }

        


        #endregion

        #region Http Headers
        private Dictionary<string, string> mHttpHeaders;

        public Dictionary<string, string> HttpHeaders
        {
            get { return mHttpHeaders; }
            set { mHttpHeaders = value; }
        }
        #endregion

        #region Валидность/Невалидность

        private List<string> mValidRegexes;

        public List<string> ValidRegexes
        {
            get { return mValidRegexes; }
            set { mValidRegexes = value; }
        }

        private List<string> mInvalidRegexes;

        public List<string> InvalidRegexes
        {
            get { return mInvalidRegexes; }
            set { mInvalidRegexes = value; }
        }


        #endregion

        #endregion

        #region Constructor

        public DatacolHttp(int _mTimeout = 10000,
            bool _mAllowAutoRedirect = true,
            int _mAttemptsToLoad = 1,
            int _mAttemptsToDownloadFile = 1,
            List<WebProxy> _mProxyList = null,
            bool _mProxyRandomly = false,
            List<string> _mUserAgentList = null,
            bool _mUserAgentsRandomly = false,
            List<string> _mRefererList = null,
            bool _mReferersRandomly = false,
            Dictionary<string, string> _mHttpHeaders = null,
            List<string> _mValidRegexes = null,
            List<string> _mInvalidRegexes = null,
            bool _mHandleCookies = false,
            bool _mSaveCookiesToFile = false,
            string _mCookiesRootFolder = "",
            string _mApplicationName = "Datacol5",
            string _mTaskName = "default",
            List<string> _mAuthCookies = null)
        {
            mHandleCookies = _mHandleCookies;

            if (mHandleCookies && _mSaveCookiesToFile)
            {
                if (_mCookiesRootFolder == "")
                {
                    _mCookiesRootFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }

                _mCookiesRootFolder = Path.Combine(_mCookiesRootFolder, StringHandling.RemoveBadCharactersFromPath(_mApplicationName, "_"));
                _mCookiesRootFolder = Path.Combine(_mCookiesRootFolder, StringHandling.RemoveBadCharactersFromPath(_mTaskName, "_"));

                if (!Directory.Exists(_mCookiesRootFolder))
                {
                    Directory.CreateDirectory(_mCookiesRootFolder);
                }
            }
            ipToCookieDict = new IpToCookieCollectionDictionary(_mSaveCookiesToFile, _mCookiesRootFolder);

            mAuthCookies = _mAuthCookies;
            if (mAuthCookies == null)
            {
                mAuthCookies = new List<string>();
            }

            mProxyListCounter = 0;
            mUserAgentCounter = 0;
            mReferersListCounter = 0;

            mTimeout = _mTimeout;
            mAllowAutoRedirect = _mAllowAutoRedirect;
            mAttemptsToLoad = _mAttemptsToLoad;
            mAttemptsToDownloadFile = _mAttemptsToDownloadFile;

            mProxyList = _mProxyList;
            if (mProxyList == null)
            {
                mProxyList = new List<WebProxy>();
            }

            mProxyRandomly = _mProxyRandomly;
            mUserAgentList = _mUserAgentList;
            if (mUserAgentList == null)
            {
                mUserAgentList = new List<string>();
            }

            mUserAgentsRandomly = _mUserAgentsRandomly;
            mRefererList = _mRefererList;
            if (mRefererList == null)
            {
                mRefererList = new List<string>();
            }

            mReferersRandomly = _mReferersRandomly;

            mHttpHeaders = _mHttpHeaders;
            if (mHttpHeaders == null)
            {
                mHttpHeaders = new Dictionary<string, string>();
            }
            mValidRegexes = _mValidRegexes;
            if (mValidRegexes == null)
            {
                mValidRegexes = new List<string>();
            }
            mInvalidRegexes = _mInvalidRegexes;
            if (mInvalidRegexes == null)
            {
                mInvalidRegexes = new List<string>();
            }

            mProxy = null;
            mUserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2) Gecko/20100115 MRA 5.6 (build 03278) Firefox/3.6 (.NET CLR 3.5.30729)";



        }

        #endregion

        #region Methods

        private byte[] LoadImageNative2(string url, string referer, out string error, WebProxy usedProxy)
        {
            error = "";
            var w = new WebClient();

            if (!String.IsNullOrEmpty(referer))
            {
                w.Headers.Add("Referer", referer);
            }


            ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
            var res = new byte[0];
            try
            {
                if (usedProxy != null)
                {
                    w.Proxy = usedProxy;
                }
                else
                {
                    w.Proxy = Proxy;
                }
                res = w.DownloadData(url);
                return res;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return res;
            }
        }


        /// <summary>
        /// Загрузить картинку с сервера в байтовый массив
        /// </summary>
        /// <param name="url">URL картинки</param>
        /// <param name="referer">Referer</param>
        /// <returns></returns>
        public byte[] loadImageToBytesArray2(string url, string referer, out string error, WebProxy usedProxy)
        {

            return DownloadImageNAttempts2(url, referer, out error, usedProxy);
        }

        private byte[] DownloadImageNAttempts2(string url, string referer, out string error, WebProxy usedProxy)
        {
            error = "";
            try
            {
                int curattempts = mAttemptsToDownloadFile;

                byte[] s = null;

                while (true)
                {
                    if (curattempts <= 0) break;


                    #region Anonymity handling
                    string finalReferer = referer;

                    lock (countersLocker)
                    {
                        #region Proxy
                        if (usedProxy == null)
                        {
                            if (mProxyList.Count > 0)
                            {
                                if (mProxyRandomly)
                                {
                                    Proxy = mProxyList[NumberHandling.GetRandomValueInRange(0, mProxyList.Count - 1)];
                                }
                                else
                                {
                                    mProxyListCounter++;
                                    if (mProxyListCounter >= mProxyList.Count)
                                    {
                                        mProxyListCounter = 0;
                                    }

                                    Proxy = mProxyList[mProxyListCounter];
                                }
                            }
                        }
                        #endregion

                        #region UA
                        if (mUserAgentList.Count > 0)
                        {
                            if (mUserAgentsRandomly)
                            {
                                UserAgent = mUserAgentList[NumberHandling.GetRandomValueInRange(0, mUserAgentList.Count - 1)];
                            }
                            else
                            {
                                mUserAgentCounter++;
                                if (mUserAgentCounter >= mUserAgentList.Count)
                                {
                                    mUserAgentCounter = 0;
                                }

                                UserAgent = mUserAgentList[mUserAgentCounter];
                            }
                        }
                        #endregion

                        #region Referer
                        if (finalReferer == "")
                        {
                            if (mRefererList.Count > 0)
                            {
                                if (mReferersRandomly)
                                {
                                    finalReferer = mRefererList[NumberHandling.GetRandomValueInRange(0, mRefererList.Count - 1)];
                                }
                                else
                                {
                                    mReferersListCounter++;

                                    if (mReferersListCounter >= mRefererList.Count)
                                    {
                                        mReferersListCounter = 0;
                                    }

                                    finalReferer = mRefererList[mReferersListCounter];
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    s = LoadImageNative2(url, referer, out error, usedProxy);

                    if (error != "")
                    {
                        curattempts--;
                        continue;
                    }

                    return s;
                }

                return s;
            }

            catch (Exception exp)
            {
                error = exp.Message;
                return null;
            }
        }


        #region cookie methods
        public List<string> getCookiesList(string proxy = "")
        {
            if (proxy == "")
            {
                proxy = IpToCookieCollectionDictionary.noproxyindex;


            }

            return ipToCookieDict.getCookieCollectionToListString(proxy);
        }

        public void setCookiesLIst(List<string> cookies, string proxy = "")
        {
            ipToCookieDict.setCookieCollectionToListString(proxy, cookies);
        }

        public void saveCookiesToFile(out string error)
        {
            error = "";
            try
            {
                ipToCookieDict.IpToCookieCollectionDictionarySerialize();
            }
            catch (Exception exp)
            {
                error = exp.Message;

            }
        }
        #endregion

        #region request webpage

        /// <summary>
        /// Фукнция для загрузки вебстраницы GET (по умолчанию) или POST методом
        /// </summary>

        /// <returns></returns>
        public string request(string link, string referer, out Dictionary<string, object> outDictParams,
            out string error, string postdata = "",
            string encoding = "", WebProxy predefinedProxy = null)
        {
            error = string.Empty;
            outDictParams = new Dictionary<string, object>();
            outDictParams.Add("respcode", "");
            outDictParams.Add("totaltime", "");
            outDictParams.Add("location", "");
            outDictParams.Add("usedproxy", "");

            try
            {
                int curattempts = mAttemptsToLoad;
                
                string s = "";

                while (true)
                {
                    if (curattempts <= 0) break;

                    #region Anonymity handling
                    string finalReferer = referer;

                    lock (countersLocker)
                    {
                        #region Proxy
                        if (mProxyList.Count > 0)
                        {
                            if (mProxyRandomly)
                            {
                                Proxy = mProxyList[NumberHandling.GetRandomValueInRange(0, mProxyList.Count - 1)];
                            }
                            else
                            {
                                mProxyListCounter++;
                                if (mProxyListCounter >= mProxyList.Count)
                                {
                                    mProxyListCounter = 0;
                                }

                                Proxy = mProxyList[mProxyListCounter];
                            }
                        }
                        #endregion

                        #region UA
                        if (mUserAgentList.Count > 0)
                        {
                            if (mUserAgentsRandomly)
                            {
                                UserAgent = mUserAgentList[NumberHandling.GetRandomValueInRange(0, mUserAgentList.Count - 1)];
                            }
                            else
                            {
                                mUserAgentCounter++;
                                if (mUserAgentCounter >= mUserAgentList.Count)
                                {
                                    mUserAgentCounter = 0;
                                }

                                UserAgent = mUserAgentList[mUserAgentCounter];
                            }
                        }
                        #endregion

                        #region Referer
                        if (finalReferer == "")
                        {
                            if (mRefererList.Count > 0)
                            {
                                if (mReferersRandomly)
                                {
                                    finalReferer = mRefererList[NumberHandling.GetRandomValueInRange(0, mRefererList.Count - 1)];
                                }
                                else
                                {
                                    mReferersListCounter++;

                                    if (mReferersListCounter >= mRefererList.Count)
                                    {
                                        mReferersListCounter = 0;
                                    }

                                    finalReferer = mRefererList[mReferersListCounter];
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                    DateTime dt1 = DateTime.Now;

                    string respcode = "";
                    string Location = "";
                    WebProxy usedProxy = null;

                    s = GetReguestNative(link, finalReferer, out usedProxy, out error, out respcode, out Location, postdata, encoding,
                        Proxy);

                    double requestTotalTimeSeconds = Math.Round((DateTime.Now - dt1).TotalSeconds, 2);

                    outDictParams["respcode"] = respcode;
                    outDictParams["location"] = Location;
                    outDictParams["usedproxy"] = usedProxy;
                    outDictParams["totaltime"] = requestTotalTimeSeconds.ToString();

                    #region Валидность/невалидность
                    if (error == "")
                    {
                        if (mValidRegexes.Count > 0)
                        {
                            bool found = false;
                            foreach (string r in mValidRegexes)
                            {
                                if (Regex.IsMatch(s, r, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                                {
                                    found = true;
                                    continue;
                                }
                            }

                            if (!found)
                            {
                                error = "Не соответствие шаблону валидности";
                            }
                        }
                    }

                    if (error == "")
                    {
                        foreach (string r in mInvalidRegexes)
                        {
                            if (Regex.IsMatch(s, r, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                            {
                                error = "Соответствие шаблону НЕвалидности";
                                break;
                            }
                        }
                    }
                    #endregion

                    if (error != "")
                    {
                        curattempts--;
                        continue;
                    }

                    return s;
                }



                return s;
            }
            catch (Exception exp)
            {
                error = exp.Message;
                return "";
            }
        }

       
        //set
        private string GetReguestNative(string link, string referer, out WebProxy usedProxy, out string error, out string respcode, out string Location,
           string postdata = "", string encoding = "", WebProxy predefinedProxy = null)
        {
            Location = string.Empty;
            respcode = string.Empty;
            error = string.Empty;
            ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
            usedProxy = null;

            try
            {
                var myHttpWebRequest = (HttpWebRequest)WebRequest.Create(link);

                if (predefinedProxy != null)
                {
                    myHttpWebRequest.Proxy = predefinedProxy;
                    usedProxy = predefinedProxy;
                }
                else
                {
                    myHttpWebRequest.Proxy = mProxy;
                    usedProxy = mProxy;
                }



                myHttpWebRequest.UserAgent = mUserAgent;
                myHttpWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                myHttpWebRequest.Headers.Add("Accept-Language", "ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3");
                myHttpWebRequest.Headers.Add("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.7");
                myHttpWebRequest.Referer = referer;
                myHttpWebRequest.Timeout = mTimeout;

                foreach (string h in HttpHeaders.Keys)
                {
                    if (myHttpWebRequest.Headers[h] == null)
                    {
                        myHttpWebRequest.Headers.Add(h, HttpHeaders[h]);
                    }
                    else
                    {
                        myHttpWebRequest.Headers[h] = HttpHeaders[h];


                    }
                }

                myHttpWebRequest.ServicePoint.Expect100Continue = false;
                myHttpWebRequest.AllowAutoRedirect = mAllowAutoRedirect;


                #region Index for cookies
                string ipindexstr = IpToCookieCollectionDictionary.noproxyindex;

                if (mProxy != null)
                {
                    ipindexstr = mProxy.Address.ToString();
                }
                #endregion

                if (mHandleCookies)
                {

                    #region handle cookies

                    myHttpWebRequest.CookieContainer = new CookieContainer();
                    lock (cookieLocker)
                    {
                        CookieCollection cc = ipToCookieDict.getFromDictionary(ipindexstr);
                        myHttpWebRequest.CookieContainer.Add(cc);

                        #region handle authorization

                        if (mAuthCookies.Count > 0)
                        {
                            #region find cookies for current domain

                            if (cc.Count == 0)
                            {
                                ipToCookieDict.setCookieCollectionToListString(ipindexstr, mAuthCookies);

                                cc = ipToCookieDict.getFromDictionary(ipindexstr);
                                myHttpWebRequest.CookieContainer.Add(cc);
                            }

                            #endregion
                        }

                        #endregion
                    }


                    #endregion
                }


                if (postdata == "")
                {
                    myHttpWebRequest.Method = "GET";
                }
                else
                {
                    myHttpWebRequest.Method = "POST";
                    myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    byte[] byteArr = Encoding.GetEncoding("utf-8").GetBytes(postdata);
                    myHttpWebRequest.ContentLength = byteArr.Length;
                    myHttpWebRequest.GetRequestStream().Write(byteArr, 0, byteArr.Length);
                }

                HttpWebResponse myHttpWebResponse;

                try
                {
                    myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                    try
                    {
                        respcode = ((int)myHttpWebResponse.StatusCode).ToString();
                    }
                    catch
                    {

                    }
                }
                catch (WebException err)
                {
                    if (err.Response == null)
                    {
                        if (err.Status != null)
                        {
                            if (String.Compare(err.Status.ToString(), "NameResolutionFailure", true) == 0)
                            {
                                respcode = "no such host";
                            }
                            else
                            {
                                respcode = err.Status.ToString();
                            }
                        }
                        error = err.Message;
                        return "";
                    }
                    myHttpWebResponse = (HttpWebResponse)err.Response;
                    try
                    {
                        respcode = ((int)myHttpWebResponse.StatusCode).ToString();
                        if (respcode.Contains("403"))
                        {
                            error = "Ошибка 403";

                            return error;
                        }
                    }
                    catch
                    {

                    }
                }

                if (HandleCookies)
                {
                    lock (cookieLocker)
                    {
                        ipToCookieDict.addToDictionary(ipindexstr, myHttpWebResponse.Cookies);
                    }
                }

                Location = myHttpWebResponse.Headers["Location"] ?? "";
                //////////////////////////
                var s = "";
                Stream responseStream = myHttpWebResponse.GetResponseStream();
                StreamReader myStreamReader = null;
                if (String.IsNullOrEmpty(encoding))
                {
                    if (String.Compare(myHttpWebResponse.CharacterSet, "windows-1251", true) == 0 ||
                        String.Compare(myHttpWebResponse.CharacterSet, "ISO-8859-1", true) == 0 ||
                         String.Compare(myHttpWebResponse.CharacterSet, "cp1251", true) == 0)
                    {
                        myStreamReader = new StreamReader(responseStream, Encoding.GetEncoding(1251));
                        s = myStreamReader.ReadToEnd();
                    }
                    else if (String.Compare(myHttpWebResponse.CharacterSet, "UTF-8", true) == 0)
                    {
                        myStreamReader = new StreamReader(responseStream);
                        s = myStreamReader.ReadToEnd();
                    }
                    else
                    {
                        myStreamReader = new StreamReader(responseStream, Encoding.GetEncoding(myHttpWebResponse.CharacterSet));
                        s = myStreamReader.ReadToEnd();
                    }
                }
                else
                {
                    myStreamReader = new StreamReader(responseStream, Encoding.GetEncoding(encoding));
                    s = myStreamReader.ReadToEnd();
                }
                //////////////////////
                //var myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream(), Encoding.GetEncoding(defaultEncoding));
                //var s = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myStreamReader.Dispose();
                myHttpWebRequest.Abort();
                myHttpWebResponse.Close();

                if (String.Compare(respcode, "407", true) == 0)
                {
                    error = "Ошибка 407";
                }

                return s;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "";
            }
        }

       
        #endregion

        #region extra needed
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate,
          X509Chain chain,
          SslPolicyErrors sslPolicyErrors)
        {
            return true; // сюда даже не попадает, если сертификат неверный.  Если подписанный нормально - проходит.
        }
        #endregion

        #endregion
    }
}
