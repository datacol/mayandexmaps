﻿//#define DEVMODE
//#define SAVEDATAMODE

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using LowLevel;
using MAPrimitives.LoadManagementFolder.LoaderRelatedInterfaceFolder;
using MAPrimitives.OptionFolder;
using MATemplate.ContactParser;
using MATemplate.CoreFolder.APIClasses;
using MATemplate.EngineFolder;
using Newtonsoft.Json;

namespace MATemplate.CoreFolder
{
    public class YandexMapsApi
    {
        private string csrfToken = string.Empty;
        private Dictionary<string, object> outDictParams = new Dictionary<string, object>();
        private WebProxy webproxy = new WebProxy();
        private DatacolHttp http = new DatacolHttp(15000);
        private string error = string.Empty;
        public Dictionary<int, string> YandexCities = new Dictionary<int, string>();
        private double sdvig = 0.4375; //0,25=20 км
        private double BaseCoordinatesLat = 0;
        private double BaseCoordinatesLong = 0;
        private List<string> globallistData = new List<string>();
        private List<string> foundID = new List<string>();
        public List<Item> Items = new List<Item>();
        public int radiuskm = 20;
        // AddResultsToUIDelegate AddResultsToUIDelegate;
        public DataTable dtCityReq = null;
        //PagecodeCacher Cacher;
        private string Queue;
        private string city;
        private string globtesturl = string.Empty;
        private readonly ILoader _loader;
        private readonly CustomOptions _customOptions;

        private static bool _collectEmail;

        public YandexMapsApi() //, PagecodeCacher _Cacher)
        {
            InitCities();
        }

        public YandexMapsApi(OptionWrapper optionWrapper, string _Queue, string _city) //, PagecodeCacher _Cacher)
        {
            HashSet.Clear();
            _getTokenCount = 0;
            
            Queue = _Queue;
            city = _city;
            //Cacher = _Cacher;
            _customOptions = optionWrapper.Options as CustomOptions;
            if (_customOptions != null)
            {
                _collectEmail = _customOptions.CollectEmail;

                //var userAgentList = _customOptions.LoaderSettingWrapper.LoaderSetting.UserAgentList;
                //userAgentList = userAgentList == null || userAgentList.All(string.IsNullOrWhiteSpace)
                //    ? new List<string>()
                //    : userAgentList;

                //LoaderFactory loaderFactory;
                //var anonymityLoaderManager = new AnonymityLoaderManager(
                //    _customOptions.LoaderSettingWrapper.LoaderSetting.UseProxy
                //        ? WebProxyHandling.GetWebProxiesListFromStringList(_customOptions.LoaderSettingWrapper.LoaderSetting.ProxyList)
                //        : new List<WebProxy>(), userAgentList);

                //var browserSettings = _customOptions.LoaderSettingWrapper.LoaderSetting as BrowserLoaderSetting;
                //if (browserSettings == null)
                //{
                //    return;
                //}

                //loaderFactory = new GeckoWebLoaderFactory(_customOptions.LoaderSettingWrapper.LoaderSetting.Timeout, null, Application.ProductName,
                //    browserSettings.ShowBrowserForm,
                //    browserSettings.DisableImages,
                //    browserSettings.DisableJavascript,
                //    null);
                //var loaderBundle = new GeckoWebLoaderBundle(Application.ProductName, "", anonymityLoaderManager,
                //    _customOptions.ThreadNumber,
                //    loaderFactory, Application.ProductName);
                //loaderBundle.Init();
                //_loader = new LoaderWrapper(loaderBundle, _customOptions.CacheOn ? new PagecodeCacher() : null);
            }


            Init();
        }

        public void SetProxyList(List<string> proxyList)
        {
            http.ProxyRandomly = true;
            http.ProxyList = extra.getWebProxiesListFromStringsList(proxyList);

            if (http.ProxyList.Count == 0) throw new Exception("Список прокси пуст");
        }

        private static uint _getTokenCount = 0;

        private static readonly object SyncObj = new object();

        public List<string> GetData()
        {
            string retData = string.Empty;
            //получение кода города
            string citycode = YandexCities.FirstOrDefault(x => x.Value == city).Key.ToString();

            #region Запрос по центру города для получения данных + получение координат и количества результатов

            string reqUrl = string.Concat("https://yandex.ru/maps/api/search?text=", Queue + " " + city, "&lang=ru_RU&yandex_gid=", citycode, "&origin=maps-mouse&results=500&csrfToken=", csrfToken,
                "&z=10");
            globtesturl = reqUrl;
#if DEVMODE
           try 
	{
        retData = File.ReadAllText("city-data-cache.txt");
	}
	catch (Exception)
	{
		
	}
#else
            retData = http.request(reqUrl, "https://yandex.ru/maps/api/", out outDictParams, out error);

            if (!String.IsNullOrEmpty(error)) throw new Exception(error);
#endif


#if SAVEDATAMODE

            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "YandexMapsFiles");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            string filename = DateTime.Now.ToString("MM-dd-HH-mm-ss") + ".txt";

            File.WriteAllText(Path.Combine(path, filename), retData);
#endif

            string errorMessage = extra.gra(retData.ToString(), "message\":\"(Invalid csrf token)\"");

            #endregion

            if (csrfToken == "" || errorMessage != "")
            {
                lock (SyncObj)
                {
                    _getTokenCount++;
                    if (_customOptions.TokenGetCounter < _getTokenCount)
                    {
                        throw new Exception("Превышено количество запросов на получение токена");
                    }

                    Thread.Sleep((int) _customOptions.TokenGetDelay);
                }
                
                GetToken();
                GetData();
                Debug.WriteLine("GetToken");
            }

            _getTokenCount = 0;

            //Получение координат(для Москвы почему то другая выдача данных
            try
            {
                BaseCoordinatesLat = Convert.ToDouble(extra.gra(retData.ToString(), @"coordinates"":\[[0-9\.]*?,([0-9\.]*?)\]").Replace('.', ','));
                BaseCoordinatesLong = Convert.ToDouble(extra.gra(retData.ToString(), @"coordinates"":\[([0-9\.]*?),").Replace('.', ','));
            }
            catch (Exception)
            {
                if (city == "Москва")
                {
                    BaseCoordinatesLat = 55.755814;
                    BaseCoordinatesLong = 37.617635;

                }
            }
            //Получение количества результатов ( бывают 2 выдачи получаем большее значение)
            int allresulcount = 1;
            int allresulcount2 = 1;
            try
            {
                string retcount = extra.gra(retData.ToString(), @"totalResultCount"":([0-9\.]*?),").Replace('.', ',');
                if (retcount != "")
                    allresulcount = Convert.ToInt32(retcount);
                retcount = extra.gra(retData.ToString(), @"sort.:.rank.,.found.:([0-9\.]*?),").Replace('.', ',');
                if (retcount != "")
                    allresulcount2 = Convert.ToInt32(retcount);
            }
            catch (Exception)
            {

            }
            //Добавление данных из 1го запроса в List результатов.
            dtCityReq = JsonToXmlReg(retData);

            //Задаем сдвиг от центральной координаты города ~20 км влево вверх и вправо вниз
            //string LeftTopCoord = string.Concat((BaseCoordinatesLat - 0.25).ToString().Replace(',', '.'), ",", (BaseCoordinatesLong + 0.25).ToString().Replace(',', '.'));
            // string RightDownCoord = string.Concat((BaseCoordinatesLat + 0.25).ToString().Replace(',', '.'), ",", (BaseCoordinatesLong - 0.25).ToString().Replace(',', '.'));

            #region Получение квадратов(исходя из количества результатов считаем на сколько запросов делить) - например 2000 результатов делим на 4 квадрата по 500 результатов.

            var allPoint = GetAllPoint(6, 4);
            
            #endregion
            return allPoint;
        }

        public DataTable ProcessLinkWithPoint(string coords)
        {
            string GenerateLinkWithPoint = string.Concat("https://yandex.ru/maps/api/search?text=", Queue,
                "&lang=ru_RU&sll=COORDHERE&sspn=0.5%2C0.5&ll=COORDHERE&z=11&results=500&csrfToken=", csrfToken);

            string newlink = GenerateLinkWithPoint.Replace("COORDHERE", coords).Replace(",", "%2C");

            //var result = _loader.Load(new LoaderInput(newlink)) as LoaderRetVal;
            //string retData = result.PageCode;
            string retData = http.request(newlink, "https://yandex.ru/maps/api/", out outDictParams, out error);


            if (!String.IsNullOrEmpty(error)) throw new Exception(error);

            return JsonToXmlReg(retData);
        }

        private static readonly HashSet<string> HashSet = new HashSet<string>();

        private static readonly object LockHashSet = new object();

        private static DataTable JsonToXmlReg(string json)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Наименование компании");
            dt.Columns.Add("Время работы");
            dt.Columns.Add("Параметры");
            dt.Columns.Add("Координаты");
            dt.Columns.Add("Категории");
            dt.Columns.Add("Страница компании");
            dt.Columns.Add("Адрес");
            dt.Columns.Add("VK");
            dt.Columns.Add("Facebook");
            dt.Columns.Add("Instagram");
            dt.Columns.Add("URL");
            dt.Columns.Add("Телефоны");
            if (_collectEmail)
            {
                dt.Columns.Add("Email");
            }

            var result = JsonConvert.DeserializeObject<YandexResult>(json);
            if (result != null && result.Data != null && result.Data.Items != null && result.Data.Items.Count > 0)
            {
                Parallel.ForEach(result.Data.Items, (item) =>
                {
                    try
                    {
                        var hash = item.Title + (item.Coordinates == null ? string.Empty : string.Join(",", item.Coordinates));

                        lock (LockHashSet)
                        {
                            if (HashSet.Contains(hash))
                            {
                                return;
                            }

                            HashSet.Add(hash);
                        }

                        var email = string.Empty;
                        if (_collectEmail)
                        {
                            try
                            {
                                var urls = item.Urls;
                                if (urls != null && urls.Count > 0)
                                {
                                    var emailLoader = new EmailLoader(urls[0], 1);
                                    var emails = emailLoader.Load();
                                    if (emails != null && emails.Count > 0)
                                    {
                                        email = string.Join(",", emails);
                                    }
                                }
                            }
                            catch (Exception exception)
                            {
                                Debug.WriteLine(exception.Message);
                            }
                        }

                        lock (ParallelLock)
                        {
                            var dataRow = dt.NewRow();
                            dataRow[0] = item.Title;
                            dataRow[1] = item.WorkingTimeText;
                            dataRow[2] = FeaturesToString(item.Features);
                            dataRow[3] = item.Coordinates == null ? string.Empty : string.Join(",", item.Coordinates);
                            dataRow[4] = item.Categories == null ? string.Empty : string.Join(",", item.Categories.Select(v => v.Value));
                            dataRow[5] = string.Concat("https://yandex.ru/maps/org/", item.Seoname, "/", Regex.Replace(item.Uri, "^.*?oid=", "", RegexOptions.IgnoreCase | RegexOptions.Singleline));
                            dataRow[6] = item.Address;
                            dataRow[7] = item.SocialLinks == null ? string.Empty : string.Join(",", item.SocialLinks.Where(x => x.Name == "ВКонтакте").Select(x => x.Href));
                            dataRow[8] = item.SocialLinks == null ? string.Empty : string.Join(",", item.SocialLinks.Where(x => x.Name == "Facebook").Select(x => x.Href));
                            dataRow[9] = item.SocialLinks == null ? string.Empty : string.Join(",", item.SocialLinks.Where(x => x.Name == "Instagram").Select(x => x.Href));
                            dataRow[10] = item.Urls == null ? string.Empty : string.Join(",", item.Urls);
                            dataRow[11] = item.Phones == null ? string.Empty : string.Join(",", item.Phones.Select(x => x.Number));
                            if (_collectEmail)
                            {
                                dataRow[12] = email;
                            }

                            dt.Rows.Add(dataRow);
                        }
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine(exception.Message);
                    }
                });
            }

            return dt;
        }

        private static readonly object ParallelLock = new object();

        private static string FeaturesToString(IList<Feature> features)
        {
            var retValue = new StringBuilder();

            foreach (var feature in features)
            {
                switch (feature.Type)
                {
                    case "bool":
                        retValue.Append(string.Format("{0} :{1};", feature.Name, feature.Value.ToString().Equals("true", StringComparison.OrdinalIgnoreCase) ? "есть" : "нет"));
                        break;
                    case "enum":
                        var list = JsonConvert.DeserializeObject<List<string>>(feature.Value.ToString());
                        if (list != null && list.Count > 0)
                        {
                            retValue.Append(string.Format("{0} :{1};", feature.Name, string.Join(",", list)));
                        }
                        break;
                    default:
                        retValue.Append(string.Format("{0} :{1};", feature.Name, feature.Value));
                        break;
                }
            }

            return retValue.ToString();
        }

        private List<string> GetAllPoint(int Width, int Height)
        {
            List<string> points = new List<string>();
            double all = sdvig*2;
            double partLat = all/Width;
            double partLong = all/Height;
            double startPointLat = BaseCoordinatesLat - sdvig + partLat/2;
            double startPointLong = BaseCoordinatesLong + sdvig - partLong/2;

            string addCoord = string.Empty;
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    addCoord = string.Concat((startPointLong - partLong*j).ToString().Replace(',', '.'), ",", (startPointLat + partLat*i).ToString().Replace(',', '.'));
                    points.Add(addCoord);
                }
            }
            return points;
        }

        private void Init()
        {
            InitCities();
            http.HandleCookies = true;
            GetToken();

            switch (radiuskm)
            {
                case 5:
                    sdvig = 0.0625;
                    break;
                case 10:
                    sdvig = 0.125;
                    break;
                case 15:
                    sdvig = 0.1875;
                    break;
                case 20:
                    sdvig = 0.25;
                    break;
                case 25:
                    sdvig = 0.3125;
                    break;
                case 30:
                    sdvig = 0.375;
                    break;
                case 35:
                    sdvig = 0.4375;
                    break;
                case 40:
                    sdvig = 0.5;
                    break;
                case 45:
                    sdvig = 0.5625;
                    break;
                case 50:
                    sdvig = 0.625;
                    break;
                case 55:
                    sdvig = 0.6875;
                    break;
                case 60:
                    sdvig = 0.75;
                    break;
                case 65:
                    sdvig = 0.8125;
                    break;
                case 70:
                    sdvig = 0.875;
                    break;
                default:
                    break;
            }
        }

        private void GetToken()
        {

            string content = http.request("https://yandex.ru/maps/api/", "https://yandex.ru/maps/api/", out outDictParams, out error);
            // var result = _loader.Load(new LoaderInput("https://yandex.ru/maps/api/")) as LoaderRetVal;
            //   string content = result.PageCode;


            csrfToken = extra.gra(content, "\"csrfToken\":\"([^\"]*?)\"");

        }


        private void InitCities()
        {
            //YandexCities.Add(0, "Регионы");
            YandexCities.Add(1, "Москва");
            //YandexCities.Add(1, "Москва и область");
            YandexCities.Add(2, "Санкт-Петербург");
            // YandexCities.Add(3, "Центр");
            YandexCities.Add(4, "Белгород");
            YandexCities.Add(5, "Иваново");
            YandexCities.Add(6, "Калуга");
            YandexCities.Add(7, "Кострома");
            YandexCities.Add(8, "Курск");
            YandexCities.Add(9, "Липецк");
            YandexCities.Add(10, "Орел");
            YandexCities.Add(11, "Рязань");
            YandexCities.Add(12, "Смоленск");
            YandexCities.Add(13, "Тамбов");
            YandexCities.Add(14, "Тверь");
            YandexCities.Add(15, "Тула");
            YandexCities.Add(16, "Ярославль");
            //YandexCities.Add(17, "Северо-Запад");
            YandexCities.Add(18, "Петрозаводск");
            YandexCities.Add(19, "Сыктывкар");
            YandexCities.Add(20, "Архангельск");
            YandexCities.Add(21, "Вологда");
            YandexCities.Add(22, "Калининград");
            YandexCities.Add(23, "Мурманск");
            YandexCities.Add(24, "Великий Новгород");
            YandexCities.Add(25, "Псков");
            //YandexCities.Add(26, "Юг");
            YandexCities.Add(28, "Махачкала");
            YandexCities.Add(30, "Нальчик");
            YandexCities.Add(33, "Владикавказ");
            YandexCities.Add(35, "Краснодар");
            YandexCities.Add(36, "Ставрополь");
            YandexCities.Add(37, "Астрахань");
            YandexCities.Add(38, "Волгоград");
            YandexCities.Add(39, "Ростов-на-Дону");
            YandexCities.Add(40, "Поволжье");
            YandexCities.Add(41, "Йошкар-Ола");
            YandexCities.Add(42, "Саранск");
            YandexCities.Add(43, "Казань");
            YandexCities.Add(44, "Ижевск");
            YandexCities.Add(45, "Чебоксары");
            YandexCities.Add(46, "Киров");
            YandexCities.Add(47, "Нижний Новгород");
            YandexCities.Add(48, "Оренбург");
            YandexCities.Add(49, "Пенза");
            YandexCities.Add(50, "Пермь");
            YandexCities.Add(51, "Самара");
            YandexCities.Add(52, "Урал");
            YandexCities.Add(53, "Курган");
            YandexCities.Add(54, "Екатеринбург");
            YandexCities.Add(55, "Тюмень");
            YandexCities.Add(56, "Челябинск");
            YandexCities.Add(57, "Ханты-Мансийск");
            YandexCities.Add(58, "Салехард");
            YandexCities.Add(59, "Сибирь");
            YandexCities.Add(62, "Красноярск");
            YandexCities.Add(63, "Иркутск");
            YandexCities.Add(64, "Кемерово");
            YandexCities.Add(65, "Новосибирск");
            YandexCities.Add(66, "Омск");
            YandexCities.Add(67, "Томск");
            YandexCities.Add(68, "Чита");
            YandexCities.Add(73, "Дальний Восток");
            YandexCities.Add(74, "Якутск");
            YandexCities.Add(75, "Владивосток");
            YandexCities.Add(76, "Хабаровск");
            YandexCities.Add(77, "Благовещенск");
            YandexCities.Add(78, "Петропавловск-Камчатский");
            YandexCities.Add(79, "Магадан");
            YandexCities.Add(80, "Южно-Сахалинск");

            YandexCities.Add(382, "Россия/Общероссийские");
            YandexCities.Add(414, "Центр/Универсальное");
            YandexCities.Add(446, "Северо-Запад/Универсальное");
            YandexCities.Add(478, "Юг/Универсальное");
            YandexCities.Add(510, "Поволжье/Универсальное");
            YandexCities.Add(542, "Урал/Универсальное");
            YandexCities.Add(574, "Сибирь/Универсальное");
            YandexCities.Add(606, "Дальний Восток/Универсальное");
            YandexCities.Add(979, "Крым/Универсальное");
            YandexCities.Add(10174, "Санкт-Петербург и Ленинградская область");
            YandexCities.Add(10645, "Белгородская область");
            YandexCities.Add(10650, "Брянская область");
            YandexCities.Add(10658, "Владимирcкая область");
            YandexCities.Add(10672, "Воронежcкая область");
            YandexCities.Add(10687, "Ивановская область");
            YandexCities.Add(10693, "Калужская область");
            YandexCities.Add(10699, "Костромская область");
            YandexCities.Add(10705, "Курская область");
            YandexCities.Add(10712, "Липецкая область");
            YandexCities.Add(10772, "Орловская область");
            YandexCities.Add(10776, "Рязанская область");
            YandexCities.Add(10795, "Смоленская область");
            YandexCities.Add(10802, "Тамбовская область");
            YandexCities.Add(10819, "Тверская область");
            YandexCities.Add(10832, "Тульская область");
            YandexCities.Add(10841, "Ярославская область");
            YandexCities.Add(10842, "Архангельская область");
            YandexCities.Add(10853, "Вологодская область");
            YandexCities.Add(10857, "Калининградская область");
            YandexCities.Add(10897, "Мурманская область");
            YandexCities.Add(10904, "Новгородская область");
            YandexCities.Add(10926, "Псковская область");
            YandexCities.Add(10946, "Астраханская область");
            YandexCities.Add(10950, "Волгоградская область");
            YandexCities.Add(10933, "Республика Карелия");
            YandexCities.Add(10939, "Республика Коми");
            YandexCities.Add(10995, "Краснодарский край");
            YandexCities.Add(11004, "Республика Адыгея");
            YandexCities.Add(11010, "Республика Дагестан");
            YandexCities.Add(11012, "Республика Ингушетия");
            YandexCities.Add(11013, "Республика Кабардино-Балкария");
            YandexCities.Add(11015, "Республика Калмыкия");
            YandexCities.Add(11020, "Карачаево-Черкесская Республика");
            YandexCities.Add(11021, "Республика Северная Осетия-Алания");
            YandexCities.Add(11024, "Чеченская Республика");
            YandexCities.Add(11029, "Ростовская область");
            YandexCities.Add(11069, "Ставропольский край");
            YandexCities.Add(11070, "Кировская область");
            YandexCities.Add(11077, "Республика Марий Эл");
            YandexCities.Add(11079, "Нижегородская область");
            YandexCities.Add(11084, "Оренбургская область");
            YandexCities.Add(11095, "Пензенская область");
            YandexCities.Add(11108, "Пермский край");
            YandexCities.Add(11111, "Республика Башкортостан");
            YandexCities.Add(11117, "Республика Мордовия");
            YandexCities.Add(11131, "Самарская область");
            YandexCities.Add(11146, "Саратовская область");
            YandexCities.Add(11148, "Удмуртская республика");
            YandexCities.Add(11153, "Ульяновская область");
            YandexCities.Add(11156, "Чувашская республика");
            YandexCities.Add(11158, "Курганская область");
            YandexCities.Add(11162, "Свердловская область");
            YandexCities.Add(11176, "Тюменская область");
            YandexCities.Add(11193, "Ханты-Мансийский АО");
            YandexCities.Add(11225, "Челябинская область");
            YandexCities.Add(11232, "Ямало-Ненецкий АО");
            YandexCities.Add(11235, "Алтайский край");
            YandexCities.Add(11266, "Иркутская область");
            YandexCities.Add(11282, "Кемеровская область");
            YandexCities.Add(11309, "Красноярский край");
            YandexCities.Add(11316, "Новосибирская область");
            YandexCities.Add(11318, "Омская область");
            YandexCities.Add(11330, "Республика Бурятия");
            YandexCities.Add(11340, "Республика Хакасия");
            YandexCities.Add(11353, "Томская область");
            YandexCities.Add(11375, "Амурская область");
            YandexCities.Add(11398, "Камчатский край");
            YandexCities.Add(11403, "Магаданская область");
            YandexCities.Add(11409, "Приморский край");
            YandexCities.Add(11443, "Республика Саха (Якутия)");
            YandexCities.Add(11450, "Сахалинская область");
            YandexCities.Add(11457, "Хабаровский край");



            // YandexCities.Add(84, "США");
            YandexCities.Add(86, "Атланта");
            YandexCities.Add(87, "Вашингтон");
            YandexCities.Add(89, "Детройт");
            YandexCities.Add(90, "Сан-Франциско");
            YandexCities.Add(91, "Сиэтл");
            //YandexCities.Add(93, "Аргентина");
            //YandexCities.Add(94, "Бразилия");
            //YandexCities.Add(95, "Канада");
            //YandexCities.Add(96, "Германия");
            YandexCities.Add(97, "Гейдельберг");
            YandexCities.Add(98, "Кельн");
            YandexCities.Add(99, "Мюнхен");
            YandexCities.Add(100, "Франкфурт-на-Майне");
            YandexCities.Add(101, "Штутгарт");
            //YandexCities.Add(102, "Великобритания");
            //YandexCities.Add(111, "Европа");
            //YandexCities.Add(113, "Австрия");
            //YandexCities.Add(114, "Бельгия");
            //YandexCities.Add(115, "Болгария");
            //YandexCities.Add(116, "Венгрия");
            //YandexCities.Add(117, "Литва");
            //YandexCities.Add(118, "Нидерланды");
            //YandexCities.Add(119, "Норвегия");
            //YandexCities.Add(120, "Польша");
            //YandexCities.Add(121, "Словакия");
            //YandexCities.Add(122, "Словения");
            //YandexCities.Add(123, "Финляндия");
            //YandexCities.Add(124, "Франция");
            //YandexCities.Add(125, "Чехия");
            //YandexCities.Add(126, "Швейцария");
            //YandexCities.Add(127, "Швеция");
            //YandexCities.Add(129, "Беер-Шева");
            //YandexCities.Add(130, "Иерусалим");
            //YandexCities.Add(131, "Тель-Авив");
            //YandexCities.Add(132, "Хайфа");
            //YandexCities.Add(134, "Китай");
            //YandexCities.Add(135, "Корея");
            //YandexCities.Add(137, "Япония");
            //YandexCities.Add(138, "Австралия и Океания");
            //YandexCities.Add(139, "Новая Зеландия");
            YandexCities.Add(141, "Днепропетровск");
            YandexCities.Add(142, "Донецк");
            YandexCities.Add(143, "Киев");
            YandexCities.Add(144, "Львов");
            YandexCities.Add(145, "Одесса");
            YandexCities.Add(146, "Симферополь");
            YandexCities.Add(147, "Харьков");
            YandexCities.Add(148, "Николаев");
            YandexCities.Add(149, "Беларусь");
            YandexCities.Add(153, "Брест");
            YandexCities.Add(154, "Витебск");
            YandexCities.Add(155, "Гомель");
            YandexCities.Add(157, "Минск");
            YandexCities.Add(158, "Могилев");
            YandexCities.Add(159, "Казахстан");
            YandexCities.Add(162, "Алматы");
            YandexCities.Add(163, "Астана");
            YandexCities.Add(164, "Караганда");
            YandexCities.Add(165, "Семей");
            //YandexCities.Add(166, "СНГ");
            //YandexCities.Add(167, "Азербайджан");
            //YandexCities.Add(168, "Армения");
            //YandexCities.Add(169, "Грузия");
            //YandexCities.Add(170, "Туркмения");
            //YandexCities.Add(171, "Узбекистан");
            YandexCities.Add(172, "Уфа");
            YandexCities.Add(177, "Берлин");
            YandexCities.Add(178, "Гамбург");
            //YandexCities.Add(179, "Эстония");
            //YandexCities.Add(180, "Сербия");
            //YandexCities.Add(181, "Израиль");
            //YandexCities.Add(183, "Азия");
            //YandexCities.Add(187, "Украина");
            YandexCities.Add(190, "Павлодар");
            YandexCities.Add(191, "Брянск");
            YandexCities.Add(192, "Владимир");
            YandexCities.Add(193, "Воронеж");
            YandexCities.Add(194, "Саратов");
            YandexCities.Add(195, "Ульяновск");
            YandexCities.Add(197, "Барнаул");
            YandexCities.Add(198, "Улан-Удэ");
            YandexCities.Add(200, "Лос-Анджелес");
            YandexCities.Add(202, "Нью-Йорк");
            //YandexCities.Add(203, "Дания");
            //YandexCities.Add(204, "Испания");
            //YandexCities.Add(205, "Италия");
            //YandexCities.Add(206, "Латвия");
            //YandexCities.Add(207, "Киргизия");
            //YandexCities.Add(208, "Молдова");
            //YandexCities.Add(209, "Таджикистан");
            //YandexCities.Add(210, "Объединенные Арабские Эмираты");
            //YandexCities.Add(211, "Австралия");
            YandexCities.Add(213, "Москва");
            YandexCities.Add(214, "Долгопрудный");
            YandexCities.Add(215, "Дубна");
            YandexCities.Add(217, "Пущино");
            YandexCities.Add(219, "Черноголовка");
            YandexCities.Add(221, "Чимкент");
            YandexCities.Add(222, "Луганск");
            YandexCities.Add(223, "Бостон");
            // YandexCities.Add(225, "Россия");
            YandexCities.Add(235, "Магнитогорск");
            YandexCities.Add(236, "Набережные Челны");
            YandexCities.Add(237, "Новокузнецк");
            YandexCities.Add(238, "Новочеркасск");
            YandexCities.Add(239, "Сочи");
            YandexCities.Add(240, "Тольятти");
            //YandexCities.Add(241, "Африка");
            //YandexCities.Add(245, "Арктика и Антарктика");
            //YandexCities.Add(246, "Греция");
            //YandexCities.Add(318, "Универсальное");
            //  YandexCities.Add(349, "Москва и область/Другие города региона");
            //YandexCities.Add(350, "Москва и область/Универсальное");
            //YandexCities.Add(381, "Россия/Прочее");
            //YandexCities.Add(382, "Россия/Общероссийские");
            //YandexCities.Add(413, "Центр/Другие города региона");
            //YandexCities.Add(414, "Центр/Универсальное");
            //YandexCities.Add(445, "Северо-Запад/Другие города региона");
            //YandexCities.Add(446, "Северо-Запад/Универсальное");
            //YandexCities.Add(477, "Юг/Другие города региона");
            //YandexCities.Add(478, "Юг/Универсальное");
            //YandexCities.Add(509, "Поволжье/Другие города региона");
            //YandexCities.Add(510, "Поволжье/Универсальное");
            //YandexCities.Add(541, "Урал/Другие города региона");
            //YandexCities.Add(542, "Урал/Универсальное");
            //YandexCities.Add(573, "Сибирь/Другие города региона");
            //YandexCities.Add(574, "Сибирь/Универсальное");
            //YandexCities.Add(605, "Дальний Восток/Другие города региона");
            //YandexCities.Add(606, "Дальний Восток/Универсальное");
            //YandexCities.Add(637, "США/Прочее");
            //YandexCities.Add(638, "США/Универсальное");
            //YandexCities.Add(669, "Прочее");
            //YandexCities.Add(670, "Универсальное");
            //YandexCities.Add(701, "Германия/Прочее");
            //YandexCities.Add(702, "Германия/Универсальное");
            //YandexCities.Add(733, "Европа/Прочее");
            //YandexCities.Add(734, "Европа/Универсальное");
            //YandexCities.Add(765, "Израиль/Прочее");
            //YandexCities.Add(766, "Израиль/Универсальное");
            //YandexCities.Add(797, "Азия/Прочее");
            //YandexCities.Add(798, "Азия/Универсальное");
            //YandexCities.Add(829, "Австралия и Океания/Прочее");
            //YandexCities.Add(830, "Австралия и Океания/Универсальное");
            //YandexCities.Add(861, "Украина/Прочее");
            //YandexCities.Add(862, "Украина/Универсальное");
            //YandexCities.Add(893, "Беларусь/Прочее");
            //YandexCities.Add(894, "Беларусь/Универсальное");
            //YandexCities.Add(925, "Казахстан/Прочее");
            //YandexCities.Add(926, "Казахстан/Универсальное");
            //YandexCities.Add(957, "СНГ/Прочее");
            //YandexCities.Add(958, "СНГ/Универсальное");
            YandexCities.Add(959, "Севастополь");
            YandexCities.Add(960, "Запорожье");
            YandexCities.Add(961, "Хмельницкий");
            YandexCities.Add(962, "Херсон");
            YandexCities.Add(963, "Винница");
            YandexCities.Add(964, "Полтава");
            YandexCities.Add(965, "Сумы");
            YandexCities.Add(966, "Чернигов");
            YandexCities.Add(967, "Обнинск");
            YandexCities.Add(968, "Череповец");
            YandexCities.Add(969, "Выборг");
            YandexCities.Add(970, "Новороссийск");
            YandexCities.Add(971, "Таганрог");
            YandexCities.Add(972, "Дзержинск");
            YandexCities.Add(973, "Сургут");
            YandexCities.Add(974, "Находка");
            YandexCities.Add(975, "Бийск");
            YandexCities.Add(976, "Братск");
            // YandexCities.Add(977, "Крым");
            //YandexCities.Add(978, "Крым/Другие города региона");
            //YandexCities.Add(979, "Крым/Универсальное");
            //YandexCities.Add(980, "Страны Балтии");
            //YandexCities.Add(981, "Страны Балтии/Прочее");
            //YandexCities.Add(982, "Страны Балтии/Универсальное");
            //YandexCities.Add(983, "Турция");
            //YandexCities.Add(994, "Индия");
            //YandexCities.Add(995, "Таиланд");
            //YandexCities.Add(1004, "Ближний Восток");
            //YandexCities.Add(1048, "Канада/Прочее");
            //YandexCities.Add(1049, "Канада/Универсальное");
            //YandexCities.Add(1054, "Ближний Восток/Прочее");
            //YandexCities.Add(1055, "Ближний Восток/Универсальное");
            //YandexCities.Add(1056, "Египет");
            YandexCities.Add(1058, "Туапсе");
            YandexCities.Add(1091, "Нижневартовск");
            YandexCities.Add(1092, "Назрань");
            YandexCities.Add(1093, "Майкоп");
            YandexCities.Add(1094, "Элиста");
            YandexCities.Add(1095, "Абакан");
            YandexCities.Add(1104, "Черкесск");
            YandexCities.Add(1106, "Грозный");
            YandexCities.Add(1107, "Анапа");
            //YandexCities.Add(10002, "Северная Америка");
            //YandexCities.Add(10003, "Южная Америка");
            //YandexCities.Add(10069, "Мальта");
            //YandexCities.Add(10083, "Хорватия");
            //  YandexCities.Add(10174, "Санкт-Петербург и Ленинградская область");
            // YandexCities.Add(10176, "Ненецкий АО");
            //   YandexCities.Add(10231, "Республика Алтай");
            //  YandexCities.Add(10233, "Республика Тыва");
            // YandexCities.Add(10243, "Еврейская автономная область");
            // YandexCities.Add(10251, "Чукотский автономный округ");
            YandexCities.Add(10274, "Гродно");
            YandexCities.Add(10303, "Талдыкорган");
            YandexCities.Add(10306, "Усть-Каменогорск");
            YandexCities.Add(10313, "Кишинев");
            YandexCities.Add(10314, "Бельцы");
            YandexCities.Add(10315, "Бендеры");
            YandexCities.Add(10317, "Тирасполь");
            YandexCities.Add(10343, "Житомир");
            YandexCities.Add(10345, "Ивано-Франковск");
            YandexCities.Add(10347, "Кривой Рог");
            YandexCities.Add(10355, "Ровно");
            YandexCities.Add(10357, "Тернополь");
            YandexCities.Add(10358, "Ужгород");
            YandexCities.Add(10363, "Черкассы");
            YandexCities.Add(10365, "Черновцы");
            YandexCities.Add(10366, "Мариуполь");
            YandexCities.Add(10367, "Мелитополь");
            YandexCities.Add(10369, "Белая Церковь");
            //YandexCities.Add(10645, "Белгородская область");
            YandexCities.Add(10649, "Старый Оскол");
            // YandexCities.Add(10650, "Брянская область");
            YandexCities.Add(10656, "Александров");
            // YandexCities.Add(10658, "Владимирcкая область");
            YandexCities.Add(10661, "Гусь-Хрустальный");
            YandexCities.Add(10664, "Ковров");
            YandexCities.Add(10668, "Муром");
            YandexCities.Add(10671, "Суздаль");
            //YandexCities.Add(10672, "Воронежcкая область");
            //YandexCities.Add(10687, "Ивановская область");
            //YandexCities.Add(10693, "Калужская область");
            //YandexCities.Add(10699, "Костромская область");
            //YandexCities.Add(10705, "Курская область");
            //YandexCities.Add(10712, "Липецкая область");
            YandexCities.Add(10716, "Балашиха");
            YandexCities.Add(10719, "Видное");
            YandexCities.Add(10723, "Дмитров");
            YandexCities.Add(10725, "Домодедово");
            YandexCities.Add(10733, "Клин");
            YandexCities.Add(10734, "Коломна");
            YandexCities.Add(10735, "Красногорск");
            YandexCities.Add(10738, "Люберцы");
            YandexCities.Add(10740, "Мытищи");
            YandexCities.Add(10742, "Ногинск");
            YandexCities.Add(10743, "Одинцово");
            YandexCities.Add(10745, "Орехово-Зуево");
            YandexCities.Add(10746, "Павловский Посад");
            YandexCities.Add(10747, "Подольск");
            YandexCities.Add(10748, "Пушкино");
            YandexCities.Add(10750, "Раменское");
            YandexCities.Add(10752, "Сергиев Посад");
            YandexCities.Add(10754, "Серпухов");
            YandexCities.Add(10755, "Солнечногорск");
            YandexCities.Add(10756, "Ступино");
            YandexCities.Add(10758, "Химки");
            YandexCities.Add(10761, "Чехов");
            YandexCities.Add(10765, "Щелково");
            //YandexCities.Add(10772, "Орловская область");
            //YandexCities.Add(10776, "Рязанская область");
            //YandexCities.Add(10795, "Смоленская область");
            //YandexCities.Add(10802, "Тамбовская область");
            //YandexCities.Add(10819, "Тверская область");
            YandexCities.Add(10820, "Ржев");
            YandexCities.Add(10830, "Новомосковск");
            // YandexCities.Add(10832, "Тульская область");
            YandexCities.Add(10837, "Переславль");
            YandexCities.Add(10838, "Ростов");
            YandexCities.Add(10839, "Рыбинск");
            YandexCities.Add(10840, "Углич");
            // YandexCities.Add(10841, "Ярославская область");
            // YandexCities.Add(10842, "Архангельская область");
            YandexCities.Add(10849, "Северодвинск");
            //YandexCities.Add(10853, "Вологодская область");
            //  YandexCities.Add(10857, "Калининградская область");
            YandexCities.Add(10867, "Гатчина");
            YandexCities.Add(10894, "Апатиты");
            //YandexCities.Add(10897, "Мурманская область");
            //YandexCities.Add(10904, "Новгородская область");
            //YandexCities.Add(10926, "Псковская область");
            YandexCities.Add(10928, "Великие Луки");
            // YandexCities.Add(10933, "Республика Карелия");
            YandexCities.Add(10937, "Сортавала");
            // YandexCities.Add(10939, "Республика Коми");
            YandexCities.Add(10945, "Ухта");
            //YandexCities.Add(10946, "Астраханская область");
            //YandexCities.Add(10950, "Волгоградская область");
            YandexCities.Add(10951, "Волжский");
            YandexCities.Add(10987, "Армавир");
            YandexCities.Add(10990, "Геленджик");
            YandexCities.Add(10993, "Ейск");
            //YandexCities.Add(10995, "Краснодарский край");
            //YandexCities.Add(11004, "Республика Адыгея");
            //YandexCities.Add(11010, "Республика Дагестан");
            //YandexCities.Add(11012, "Республика Ингушетия");
            //YandexCities.Add(11013, "Республика Кабардино-Балкария");
            //YandexCities.Add(11015, "Республика Калмыкия");
            //YandexCities.Add(11020, "Карачаево-Черкесская Республика");
            //YandexCities.Add(11021, "Республика Северная Осетия-Алания");
            //YandexCities.Add(11024, "Чеченская Республика");
            // YandexCities.Add(11029, "Ростовская область");
            YandexCities.Add(11036, "Волгодонск");
            YandexCities.Add(11043, "Каменск-Шахтинский");
            YandexCities.Add(11053, "Шахты");
            YandexCities.Add(11057, "Ессентуки");
            YandexCities.Add(11062, "Кисловодск");
            YandexCities.Add(11063, "Минеральные Воды");
            YandexCities.Add(11064, "Невинномысск");
            YandexCities.Add(11067, "Пятигорск");
            // YandexCities.Add(11069, "Ставропольский край");
            // YandexCities.Add(11070, "Кировская область");
            YandexCities.Add(11071, "Кирово-Чепецк");
            //  YandexCities.Add(11077, "Республика Марий Эл");
            // YandexCities.Add(11079, "Нижегородская область");
            YandexCities.Add(11080, "Арзамас");
            YandexCities.Add(11083, "Саров");
            // YandexCities.Add(11084, "Оренбургская область");
            YandexCities.Add(11091, "Орск");
            // YandexCities.Add(11095, "Пензенская область");
            //   YandexCities.Add(11108, "Пермский край");
            YandexCities.Add(11110, "Соликамск");
            //    YandexCities.Add(11111, "Республика Башкортостан");
            YandexCities.Add(11114, "Нефтекамск");
            YandexCities.Add(11115, "Салават");
            YandexCities.Add(11116, "Стерлитамак");
            //  YandexCities.Add(11117, "Республика Мордовия");
            YandexCities.Add(11119, "Татарстан");
            YandexCities.Add(11121, "Альметьевск");
            YandexCities.Add(11122, "Бугульма");
            YandexCities.Add(11123, "Елабуга");
            YandexCities.Add(11125, "Зеленодольск");
            YandexCities.Add(11127, "Нижнекамск");
            YandexCities.Add(11129, "Чистополь");
            // YandexCities.Add(11131, "Самарская область");
            YandexCities.Add(11132, "Жигулевск");
            YandexCities.Add(11139, "Сызрань");
            YandexCities.Add(11143, "Балаково");
            //YandexCities.Add(11146, "Саратовская область");
            YandexCities.Add(11147, "Энгельс");
            //  YandexCities.Add(11148, "Удмуртская республика");
            YandexCities.Add(11150, "Глазов");
            YandexCities.Add(11152, "Сарапул");
            //YandexCities.Add(11153, "Ульяновская область");
            YandexCities.Add(11155, "Димитровград");
            // YandexCities.Add(11156, "Чувашская республика");
            //YandexCities.Add(11158, "Курганская область");
            //YandexCities.Add(11162, "Свердловская область");
            YandexCities.Add(11164, "Каменск-Уральский");
            YandexCities.Add(11168, "Нижний Тагил");
            YandexCities.Add(11170, "Новоуральск");
            YandexCities.Add(11171, "Первоуральск");
            YandexCities.Add(11173, "Ишим");
            YandexCities.Add(11175, "Тобольск");
            // YandexCities.Add(11176, "Тюменская область");
            // YandexCities.Add(11193, "Ханты-Мансийский АО");
            YandexCities.Add(11202, "Златоуст");
            YandexCities.Add(11212, "Миасс");
            YandexCities.Add(11214, "Озерск");
            YandexCities.Add(11217, "Сатка");
            YandexCities.Add(11218, "Снежинск");
            // YandexCities.Add(11225, "Челябинская область");
            // YandexCities.Add(11232, "Ямало-Ненецкий АО");
            // YandexCities.Add(11235, "Алтайский край");
            YandexCities.Add(11251, "Рубцовск");
            YandexCities.Add(11256, "Ангарск");
            //  YandexCities.Add(11266, "Иркутская область");
            YandexCities.Add(11273, "Усть-Илимск");
            // YandexCities.Add(11282, "Кемеровская область");
            YandexCities.Add(11287, "Междуреченск");
            YandexCities.Add(11291, "Прокопьевск");
            YandexCities.Add(11302, "Ачинск");
            YandexCities.Add(11306, "Кайеркан");
            // YandexCities.Add(11309, "Красноярский край");
            YandexCities.Add(11311, "Норильск");
            YandexCities.Add(11314, "Бердск");
            // YandexCities.Add(11316, "Новосибирская область");
            //  YandexCities.Add(11318, "Омская область");
            YandexCities.Add(11319, "Горно-Алтайск");
            // YandexCities.Add(11330, "Республика Бурятия");
            YandexCities.Add(11333, "Кызыл");
            // YandexCities.Add(11340, "Республика Хакасия");
            YandexCities.Add(11341, "Саяногорск");
            YandexCities.Add(11351, "Северск");
            // YandexCities.Add(11353, "Томская область");
            YandexCities.Add(11374, "Белогорск");
            // YandexCities.Add(11375, "Амурская область");
            YandexCities.Add(11391, "Тында");
            YandexCities.Add(11393, "Биробиджан");
            // YandexCities.Add(11398, "Камчатский край");
            //  YandexCities.Add(11403, "Магаданская область");
            //   YandexCities.Add(11409, "Приморский край");
            YandexCities.Add(11426, "Уссурийск");
            //  YandexCities.Add(11443, "Республика Саха (Якутия)");
            // YandexCities.Add(11450, "Сахалинская область");
            YandexCities.Add(11453, "Комсомольск-на-Амуре");
            // YandexCities.Add(11457, "Хабаровский край");
            YandexCities.Add(11458, "Анадырь");
            YandexCities.Add(11463, "Евпатория");
            YandexCities.Add(11464, "Керчь");
            YandexCities.Add(11469, "Феодосия");
            YandexCities.Add(11470, "Ялта");
            YandexCities.Add(11471, "Алушта");
            YandexCities.Add(20040, "Выкса");
            YandexCities.Add(20044, "Кстово");
            YandexCities.Add(20086, "Железногорск");
            YandexCities.Add(20221, "Кировоград");
            YandexCities.Add(20222, "Луцк");
            YandexCities.Add(20258, "Сатис");
            //  YandexCities.Add(20271, "Мексика");
            YandexCities.Add(20273, "Актобе");
            YandexCities.Add(20523, "Электросталь");
            //YandexCities.Add(20529, "Львовская область");
            //YandexCities.Add(20530, "Закарпатская область");
            //YandexCities.Add(20531, "Тернопольская область");
            //YandexCities.Add(20532, "Украина/Ивано-Франковская область");
            //YandexCities.Add(20533, "Черновицкая область");
            //YandexCities.Add(20534, "Ровенская область");
            //YandexCities.Add(20535, "Хмельницкая область");
            //YandexCities.Add(20536, "Донецкая область");
            //YandexCities.Add(20537, "Днепропетровская область");
            //YandexCities.Add(20538, "Харьковская область");
            //YandexCities.Add(20539, "Запорожская область");
            //YandexCities.Add(20540, "Луганская область");
            //YandexCities.Add(20541, "Одесская область");
            //YandexCities.Add(20542, "Херсонская область");
            //YandexCities.Add(20543, "Николаевская область");
            //YandexCities.Add(20544, "Киевская область");
            //YandexCities.Add(20545, "Винницкая область");
            //YandexCities.Add(20546, "Черкасская область");
            //YandexCities.Add(20547, "Житомирская область");
            //YandexCities.Add(20548, "Кировоградская область");
            //YandexCities.Add(20549, "Полтавская область");
            //YandexCities.Add(20550, "Волынская область");
            //YandexCities.Add(20551, "Черниговская область");
            //YandexCities.Add(20552, "Сумская область");
            YandexCities.Add(20554, "Краматорск");
            YandexCities.Add(20571, "Жуковский");
            YandexCities.Add(20574, "Кипр");
            YandexCities.Add(20728, "Королёв");
            YandexCities.Add(20809, "Кокшетау");
            YandexCities.Add(21609, "Кременчуг");
            YandexCities.Add(21610, "Черногория");
            YandexCities.Add(21621, "Реутов");
            YandexCities.Add(21622, "Железнодорожный");
            //YandexCities.Add(21777, "Сахалинская область/Универсальное");
            //YandexCities.Add(21778, "Сахалинская область/Прочее");
            //YandexCities.Add(21779, "Приморский край/Универсальное");
            //YandexCities.Add(21780, "Приморский край/Прочее");
            //YandexCities.Add(21781, "Магаданская область/Универсальное");
            //YandexCities.Add(21782, "Магаданская область/Прочее");
            //YandexCities.Add(21783, "Еврейская автономная область/Универсальное");
            //YandexCities.Add(21784, "Еврейская автономная область/Прочее");
            //YandexCities.Add(21785, "Чукотский автономный округ/Универсальное");
            //YandexCities.Add(21786, "Чукотский автономный округ/Прочее");
            //YandexCities.Add(21787, "Республика Саха (Якутия)/Универсальное");
            //YandexCities.Add(21788, "Республика Саха (Якутия)/Прочее");
            //YandexCities.Add(21789, "Хабаровский край/Универсальное");
            //YandexCities.Add(21790, "Хабаровский край/Прочее");
            //YandexCities.Add(21791, "Амурская область/Универсальное");
            //YandexCities.Add(21792, "Амурская область/Прочее");
            //YandexCities.Add(21793, "Камчатский край/Универсальное");
            //YandexCities.Add(21794, "Камчатский край/Прочее");
            //YandexCities.Add(21796, "Алтайский край/Универсальное");
            //YandexCities.Add(21797, "Алтайский край/Прочее");
            //YandexCities.Add(21798, "Иркутская область/Универсальное");
            //YandexCities.Add(21799, "Иркутская область/Прочее");
            //YandexCities.Add(21800, "Кемеровская область/Универсальное");
            //YandexCities.Add(21801, "Кемеровская область/Прочее");
            //YandexCities.Add(21802, "Красноярский край/Универсальное");
            //YandexCities.Add(21803, "Красноярский край/Прочее");
            //YandexCities.Add(21804, "Новосибирская область/Универсальное");
            //YandexCities.Add(21805, "Новосибирская область/Прочее");
            //YandexCities.Add(21806, "Омская область/Универсальное");
            //YandexCities.Add(21807, "Омская область/Прочее");
            //YandexCities.Add(21808, "Республика Алтай/Универсальное");
            //YandexCities.Add(21809, "Республика Алтай/Прочее");
            //YandexCities.Add(21810, "Республика Бурятия/Универсальное");
            //YandexCities.Add(21811, "Республика Бурятия/Прочее");
            //YandexCities.Add(21812, "Республика Тыва/Универсальное");
            //YandexCities.Add(21813, "Республика Тыва/Прочее");
            //YandexCities.Add(21814, "Республика Хакасия/Универсальное");
            //YandexCities.Add(21815, "Республика Хакасия/Прочее");
            //YandexCities.Add(21816, "Томская область/Универсальное");
            //YandexCities.Add(21817, "Томская область/Прочее");
            //YandexCities.Add(21818, "Забайкальский край/Универсальное");
            //YandexCities.Add(21819, "Забайкальский край/Прочее");
            //YandexCities.Add(21825, "Курганская область/Универсальное");
            //YandexCities.Add(21826, "Курганская область/Прочее");
            //YandexCities.Add(21827, "Свердловская область/Универсальное");
            //YandexCities.Add(21828, "Свердловская область/Прочее");
            //YandexCities.Add(21829, "Тюменская область/Универсальное");
            //YandexCities.Add(21830, "Тюменская область/Прочее");
            //YandexCities.Add(21831, "Ханты-Мансийский АО/Универсальное");
            //YandexCities.Add(21832, "Ханты-Мансийский АО/Прочее");
            //YandexCities.Add(21833, "Челябинская область/Универсальное");
            //YandexCities.Add(21834, "Челябинская область/Прочее");
            //YandexCities.Add(21835, "Ямало-Ненецкий АО/Универсальное");
            //YandexCities.Add(21836, "Ямало-Ненецкий АО/Прочее");
            //YandexCities.Add(21837, "Кировская область/Универсальное");
            //YandexCities.Add(21838, "Кировская область/Прочее");
            //YandexCities.Add(21839, "Республика Марий Эл/Универсальное");
            //YandexCities.Add(21840, "Республика Марий Эл/Прочее");
            //YandexCities.Add(21841, "Нижегородская область/Универсальное");
            //YandexCities.Add(21842, "Нижегородская область/Прочее");
            //YandexCities.Add(21843, "Оренбургская область/Универсальное");
            //YandexCities.Add(21844, "Оренбургская область/Прочее");
            //YandexCities.Add(21845, "Пензенская область/Универсальное");
            //YandexCities.Add(21846, "Пензенская область/Прочее");
            //YandexCities.Add(21847, "Пермский край/Универсальное");
            //YandexCities.Add(21848, "Пермский край/Прочее");
            //YandexCities.Add(21849, "Республика Башкортостан/Универсальное");
            //YandexCities.Add(21850, "Республика Башкортостан/Прочее");
            //YandexCities.Add(21852, "Республика Мордовия/Универсальное");
            //YandexCities.Add(21853, "Республика Мордовия/Прочее");
            //YandexCities.Add(21854, "Татарстан/Универсальное");
            //YandexCities.Add(21855, "Татарстан/Прочее");
            //YandexCities.Add(21856, "Самарская область/Универсальное");
            //YandexCities.Add(21857, "Самарская область/Прочее");
            //YandexCities.Add(21858, "Саратовская область/Универсальное");
            //YandexCities.Add(21859, "Саратовская область/Прочее");
            //YandexCities.Add(21860, "Удмуртская республика/Универсальное");
            //YandexCities.Add(21861, "Удмуртская республика/Прочее");
            //YandexCities.Add(21862, "Ульяновская область/Универсальное");
            //YandexCities.Add(21863, "Ульяновская область/Прочее");
            //YandexCities.Add(21864, "Чувашская республика/Универсальное");
            //YandexCities.Add(21865, "Чувашская республика/Прочее");
            //YandexCities.Add(21866, "Астраханская область/Универсальное");
            //YandexCities.Add(21867, "Астраханская область/Прочее");
            //YandexCities.Add(21868, "Волгоградская область/Универсальное");
            //YandexCities.Add(21869, "Волгоградская область/Прочее");
            //YandexCities.Add(21870, "Краснодарский край/Универсальное");
            //YandexCities.Add(21871, "Краснодарский край/Прочее");
            //YandexCities.Add(21872, "Республика Адыгея/Универсальное");
            //YandexCities.Add(21873, "Республика Адыгея/Прочее");
            //YandexCities.Add(21874, "Республика Дагестан/Универсальное");
            //YandexCities.Add(21875, "Республика Дагестан/Прочее");
            //YandexCities.Add(21876, "Республика Ингушетия/Универсальное");
            //YandexCities.Add(21877, "Республика Ингушетия/Прочее");
            //YandexCities.Add(21878, "Республика Кабардино-Балкария/Универсальное");
            //YandexCities.Add(21879, "Республика Кабардино-Балкария/Прочее");
            //YandexCities.Add(21880, "Республика Калмыкия/Универсальное");
            //YandexCities.Add(21881, "Республика Калмыкия/Прочее");
            //YandexCities.Add(21882, "Карачаево-Черкесская Республика/Универсальное");
            //YandexCities.Add(21883, "Карачаево-Черкесская Республика/Прочее");
            //YandexCities.Add(21884, "Республика Северная Осетия-Алания/Универсальное");
            //YandexCities.Add(21885, "Республика Северная Осетия-Алания/Прочее");
            //YandexCities.Add(21886, "Ростовская область/Универсальное");
            //YandexCities.Add(21887, "Ростовская область/Прочее");
            //YandexCities.Add(21888, "Ставропольский край/Универсальное");
            //YandexCities.Add(21889, "Ставропольский край/Прочее");
            //YandexCities.Add(21890, "Чеченская Республика/Универсальное");
            //YandexCities.Add(21891, "Чеченская Республика/Прочее");
            //YandexCities.Add(21892, "Санкт-Петербург и Ленинградская область/Универсальное");
            //YandexCities.Add(21893, "Санкт-Петербург и Ленинградская область/Прочее");
            //YandexCities.Add(21894, "Архангельская область/Универсальное");
            //YandexCities.Add(21895, "Архангельская область/Прочее");
            //YandexCities.Add(21896, "Вологодская область/Универсальное");
            //YandexCities.Add(21897, "Вологодская область/Прочее");
            //YandexCities.Add(21898, "Калининградская область/Универсальное");
            //YandexCities.Add(21899, "Калининградская область/Прочее");
            //YandexCities.Add(21900, "Мурманская область/Универсальное");
            //YandexCities.Add(21901, "Мурманская область/Прочее");
            //YandexCities.Add(21902, "Новгородская область/Универсальное");
            //YandexCities.Add(21903, "Новгородская область/Прочее");
            //YandexCities.Add(21904, "Псковская область/Универсальное");
            //YandexCities.Add(21905, "Псковская область/Прочее");
            //YandexCities.Add(21906, "Республика Карелия/Универсальное");
            //YandexCities.Add(21907, "Республика Карелия/Прочее");
            //YandexCities.Add(21908, "Республика Коми/Универсальное");
            //YandexCities.Add(21909, "Республика Коми/Прочее");
            //YandexCities.Add(21910, "Белгородская область/Универсальное");
            //YandexCities.Add(21911, "Белгородская область/Прочее");
            //YandexCities.Add(21912, "Брянская область/Универсальное");
            //YandexCities.Add(21913, "Брянская область/Прочее");
            //YandexCities.Add(21914, "Владимирcкая область/Универсальное");
            //YandexCities.Add(21915, "Владимирcкая область/Прочее");
            //YandexCities.Add(21916, "Воронежcкая область/Универсальное");
            //YandexCities.Add(21917, "Воронежcкая область/Прочее");
            //YandexCities.Add(21918, "Ивановская область/Универсальное");
            //YandexCities.Add(21919, "Ивановская область/Прочее");
            //YandexCities.Add(21920, "Калужская область/Универсальное");
            //YandexCities.Add(21921, "Калужская область/Прочее");
            //YandexCities.Add(21922, "Костромская область/Универсальное");
            //YandexCities.Add(21923, "Костромская область/Прочее");
            //YandexCities.Add(21924, "Курская область/Универсальное");
            //YandexCities.Add(21925, "Курская область/Прочее");
            //YandexCities.Add(21926, "Липецкая область/Универсальное");
            //YandexCities.Add(21927, "Липецкая область/Прочее");
            //YandexCities.Add(21928, "Орловская область/Универсальное");
            //YandexCities.Add(21929, "Орловская область/Прочее");
            //YandexCities.Add(21930, "Рязанская область/Универсальное");
            //YandexCities.Add(21931, "Рязанская область/Прочее");
            //YandexCities.Add(21932, "Смоленская область/Универсальное");
            //YandexCities.Add(21933, "Смоленская область/Прочее");
            //YandexCities.Add(21934, "Тамбовская область/Универсальное");
            //YandexCities.Add(21935, "Тамбовская область/Прочее");
            //YandexCities.Add(21936, "Тверская область/Универсальное");
            //YandexCities.Add(21937, "Тверская область/Прочее");
            //YandexCities.Add(21938, "Тульская область/Универсальное");
            //YandexCities.Add(21939, "Тульская область/Прочее");
            //YandexCities.Add(21940, "Ярославская область/Универсальное");
            //YandexCities.Add(21941, "Ярославская область/Прочее");
            //YandexCities.Add(21942, "Универсальное");
            //YandexCities.Add(21943, "Прочее");
            // YandexCities.Add(21949, "Забайкальский край");
            YandexCities.Add(24876, "Макеевка");
            YandexCities.Add(26034, "Жодино");
            //YandexCities.Add(29386, "Абхазия");
            //YandexCities.Add(29387, "Южная Осетия");
            //YandexCities.Add(29403, "Акмолинская область");
            //YandexCities.Add(29404, "Актюбинская область");
            //YandexCities.Add(29406, "Алматинская область");
            //YandexCities.Add(29407, "Атырауская область");
            //YandexCities.Add(29408, "Восточно-Казахстанская область");
            //YandexCities.Add(29409, "Жамбылская область");
            //YandexCities.Add(29410, "Западно-Казахстанская область");
            //YandexCities.Add(29411, "Карагандинская область");
            //YandexCities.Add(29412, "Костанайская область");
            //YandexCities.Add(29413, "Кызылординская область");
            //YandexCities.Add(29414, "Мангистауская область");
            //YandexCities.Add(29415, "Павлодарская область");
            //YandexCities.Add(29416, "Северо-Казахстанская область");
            //YandexCities.Add(29417, "Южно-Казахстанская область");
            //YandexCities.Add(29629, "Могилевская область");
            //YandexCities.Add(29630, "Минская область");
            //YandexCities.Add(29631, "Гомельская область");
            //YandexCities.Add(29632, "Брестская область");
            //YandexCities.Add(29633, "Витебская область");
            //YandexCities.Add(29634, "Гродненская область");
            YandexCities.Add(33883, "Комрат");
            //YandexCities.Add(101852, "Минская область/Универсальное");
            //YandexCities.Add(101853, "Минская область/Прочее");
            //YandexCities.Add(101854, "Гомельская область/Универсальное");
            //YandexCities.Add(101855, "Гомельская область/Прочее");
            //YandexCities.Add(101856, "Витебская область/Универсальное");
            //YandexCities.Add(101857, "Витебская область/Прочее");
            //YandexCities.Add(101858, "Брестская область/Универсальное");
            //YandexCities.Add(101859, "Брестская область/Прочее");
            //YandexCities.Add(101860, "Гродненская область/Универсальное");
            //YandexCities.Add(101861, "Гродненская область/Прочее");
            //YandexCities.Add(101862, "Могилевская область/Универсальное");
            //YandexCities.Add(101863, "Могилевская область/Прочее");
            //YandexCities.Add(101864, "Киевская область/Прочее");
            //YandexCities.Add(101865, "Киевская область/Универсальное");
            //YandexCities.Add(102444, "Северный Кавказ");
            //YandexCities.Add(102445, "Северный Кавказ/Другие города региона");
            //YandexCities.Add(102446, "Северный Кавказ/Универсальное");
            //YandexCities.Add(102450, "Полтавская область/Прочее");
            //YandexCities.Add(102451, "Черкасская область/Прочее");
            //YandexCities.Add(102452, "Винницкая область/Прочее");
            //YandexCities.Add(102453, "Кировоградская область/Прочее");
            //YandexCities.Add(102454, "Житомирская область/Прочее");
            //YandexCities.Add(102455, "Харьковская область/Прочее");
            //YandexCities.Add(102456, "Донецкая область/Прочее");
            //YandexCities.Add(102457, "Днепропетровская область/Прочее");
            //YandexCities.Add(102458, "Луганская область/Прочее");
            //YandexCities.Add(102459, "Запорожская область/Прочее");
            //YandexCities.Add(102460, "Одесская область/Прочее");
            //YandexCities.Add(102461, "Николаевская область/Прочее");
            //YandexCities.Add(102462, "Херсонская область/Прочее");
            //YandexCities.Add(102464, "Львовская область/Прочее");
            //YandexCities.Add(102465, "Хмельницкая область/Прочее");
            //YandexCities.Add(102466, "Тернопольская область/Прочее");
            //YandexCities.Add(102467, "Ровенская область/Прочее");
            //YandexCities.Add(102468, "Черновицкая область/Прочее");
            //  YandexCities.Add(102469, "Прочее");
            //YandexCities.Add(102470, "Закарпатская область/Прочее");
            //YandexCities.Add(102471, "Ивано-Франковская область/Прочее");
            //YandexCities.Add(102472, "Сумская область/Прочее");
            //YandexCities.Add(102473, "Черниговская область/Прочее");
            //YandexCities.Add(102475, "Полтавская область/Универсальное");
            //YandexCities.Add(102476, "Черкасская область/Универсальное");
            //YandexCities.Add(102477, "Винницкая область/Универсальное");
            //YandexCities.Add(102478, "Кировоградская область/Универсальное");
            //YandexCities.Add(102479, "Житомирская область/Универсальное");
            //YandexCities.Add(102480, "Харьковская область/Универсальное");
            //YandexCities.Add(102481, "Донецкая область/Универсальное");
            //YandexCities.Add(102482, "Днепропетровская область/Универсальное");
            //YandexCities.Add(102483, "Луганская область/Универсальное");
            //YandexCities.Add(102484, "Запорожская область/Универсальное");
            //YandexCities.Add(102485, "Одесская область/Универсальное");
            //YandexCities.Add(102486, "Николаевская область/Универсальное");
            //YandexCities.Add(102487, "Херсонская область/Универсальное");
            //YandexCities.Add(102489, "Львовская область/Универсальное");
            //YandexCities.Add(102490, "Хмельницкая область/Универсальное");
            //YandexCities.Add(102491, "Тернопольская область/Универсальное");
            //YandexCities.Add(102492, "Ровенская область/Универсальное");
            //YandexCities.Add(102493, "Черновицкая область/Универсальное");
            //YandexCities.Add(102494, "Волынская область/Универсальное");
            //YandexCities.Add(102495, "Закарпатская область/Универсальное");
            //YandexCities.Add(102496, "Ивано-Франковская область/Универсальное");
            //YandexCities.Add(102497, "Сумская область/Универсальное");
            //YandexCities.Add(102498, "Черниговская область/Универсальное");
            //YandexCities.Add(102499, "Алматинская область/Прочее");
            //YandexCities.Add(102500, "Карагандинская область/Прочее");
            //YandexCities.Add(102501, "Акмолинская область/Прочее");
            //YandexCities.Add(102502, "Восточно-Казахстанская область/Прочее");
            //YandexCities.Add(102503, "Павлодарская область/Прочее");
            //YandexCities.Add(102504, "Костанайская область/Прочее");
            //YandexCities.Add(102505, "Западно-Казахстанская область/Прочее");
            //YandexCities.Add(102506, "Северо-Казахстанская область/Прочее");
            //YandexCities.Add(102507, "Южно-Казахстанская область/Прочее");
            //YandexCities.Add(102508, "Актюбинская область/Прочее");
            //YandexCities.Add(102509, "Атырауская область/Прочее");
            //YandexCities.Add(102510, "Мангистауская область/Прочее");
            //YandexCities.Add(102511, "Жамбылская область/Прочее");
            //YandexCities.Add(102512, "Кызылординская область/Прочее");
            //YandexCities.Add(102513, "Алматинская область/Универсальное");
            //YandexCities.Add(102514, "Карагандинская область/Универсальное");
            //YandexCities.Add(102515, "Акмолинская область/Универсальное");
            //YandexCities.Add(102516, "Восточно-Казахстанская область/Универсальное");
            //YandexCities.Add(102517, "Павлодарская область/Универсальное");
            //YandexCities.Add(102518, "Костанайская область/Универсальное");
            //YandexCities.Add(102519, "Западно-Казахстанская область/Универсальное");
            //YandexCities.Add(102520, "Северо-Казахстанская область/Универсальное");
            //YandexCities.Add(102521, "Южно-Казахстанская область/Универсальное");
            //YandexCities.Add(102522, "Актюбинская область/Универсальное");
            //YandexCities.Add(102523, "Атырауская область/Универсальное");
            //YandexCities.Add(102524, "Мангистауская область/Универсальное");
            //YandexCities.Add(102525, "Жамбылская область/Универсальное");
            //YandexCities.Add(102526, "Кызылординская область/Универсальное");
            //YandexCities.Add(115092, "Крымский федеральный округ");
            //YandexCities.Add(115674, "Молдова/Прочее");
            //YandexCities.Add(115675, "Молдова/Универсальное");

        }
    }

}