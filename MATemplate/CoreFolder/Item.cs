﻿using System.Collections.Generic;

namespace MATemplate.CoreFolder
{
    public class Item
    {
        public string id = string.Empty;
        public string name = string.Empty;
        public string url = string.Empty;
        public List<string> Phones = new List<string>();
        public string Hours = string.Empty;
        public string geometry = string.Empty;
        public string description = string.Empty;
        public string coordinates = string.Empty;
        public List<string> category = new List<string>();
        public string address = string.Empty;
        public List<string> feautures = new List<string>();
        public List<string> emails = new List<string>();
        public string companyLink = string.Empty;
        public string seoname = string.Empty;
        public string vklink = string.Empty;
        public string fblink = string.Empty;
        public string instalink = string.Empty;
    }
}